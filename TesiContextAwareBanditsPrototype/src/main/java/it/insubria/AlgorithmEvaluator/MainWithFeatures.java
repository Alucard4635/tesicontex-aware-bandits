package it.insubria.AlgorithmEvaluator;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import it.insubria.cabAlgorithm.AdvisableConsulter;
import it.insubria.cabAlgorithm.clusters.Cluster;
import it.insubria.cabAlgorithm.clusters.ClusterModelMaker;
import it.insubria.cabAlgorithm.clusters.VectorSimilarityClusterMaker;
import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.LinearMatrix;
import it.insubria.cabAlgorithm.dataStructures.FeaturesAdvisable;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.MoldableModelNode;
import it.insubria.cabAlgorithm.dataStructures.Vector;
import it.insubria.socialNetwork.DirectionalLink;
import it.insubria.cabAlgorithm.dataStructures.Seeker;

/**
 * @author Andrea Marisio<br>
 *Class for test consulter on advisable objects with features
 */
public class MainWithFeatures {

	/**
	 * the exploration parameter for consulter
	 */
	private static final double EXPLORATION_CONSTANT_ALPHA = 0.1;
	/**
	 * default link weight
	 */
	private static final double DEFAULT_WEIGHT = 0.7;

	/**
	 * @author Andrea Marisio
	 *used for change file order
	 */
	private static enum FilesType {
		ITEMS, USERS, PAYOFF, GRAPH
	}

	/**
	 * @param args like in all main methods
	 */
	public static void main(String[] args) {
		FilesType[] values = FilesType.values();
		File[] files = new File[values.length];
		JFileChooser f = new JFileChooser();
		f.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int featuresSizeTmp = 20;
		for (int i = 0; i < files.length; i++) {
			JOptionPane.showMessageDialog(null, "Select " + values[i] + " csv file");
			int retVal = f.showOpenDialog(null);
			if (retVal != JFileChooser.APPROVE_OPTION && i != FilesType.PAYOFF.ordinal()
					&& i != FilesType.USERS.ordinal() && i != FilesType.GRAPH.ordinal()) {
				System.exit(0);
			} else {
				if (retVal == JFileChooser.APPROVE_OPTION) {
					files[i] = f.getSelectedFile();
					if (i == FilesType.ITEMS.ordinal()) {
						String showInputDialog = JOptionPane.showInputDialog("numeroDiFeatures","20");
						featuresSizeTmp = Integer.parseInt(showInputDialog);

					}
				}
			}
		}
		final int featuresSize=featuresSizeTmp;

		boolean onlyOnHit = false;
		if (files[FilesType.PAYOFF.ordinal()] != null) {
			onlyOnHit = true;
			System.out.println("HIT_ONLY");
			try {
				List<String[]> readCSV = CSVHandler.readCSV(files[FilesType.PAYOFF.ordinal()]);
				double minRow=Double.MAX_VALUE;
				double maxRow=0;
				double minSum=0;
				double maxSum=0;
				double sumAll=0;
				double lines=0;
				double currentDoubleValue;
				double valuesNumber=0;
				for (String[] lineOfPayoff : readCSV) {
					lines++;
					minRow=Double.MAX_VALUE;
					maxRow=0;
					for (String value : lineOfPayoff) {
						valuesNumber++;
						currentDoubleValue = Double.parseDouble(value);
						sumAll+=currentDoubleValue;
						minRow=Math.min(minRow, currentDoubleValue);
						maxRow=Math.max(maxRow, currentDoubleValue);
					}
					minSum+=minRow;
					maxSum+=maxRow;
				}
				System.out.println(minSum+" "+maxSum+" "+sumAll);
				System.out.println(minSum/lines+" "+maxSum/lines+" "+sumAll/valuesNumber);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println(">Scansiono Tutti gli oggetti");
		try {
			File fileUSERS = files[FilesType.USERS.ordinal()];
			String lastString = "";
			if (fileUSERS!=null) {
				System.out.println(">Scansiono Tutti gli utenti");
	
				Collection<Integer> allPossibleIntegerValuesUser = CSVHandler
						.getAllPossibleIntegerValues(fileUSERS).values();
				int userNumber = 0;
				for (Integer integer : allPossibleIntegerValuesUser) {
					userNumber++;
				}
				System.out.println("NumberOfUsers=" + userNumber);
			}

			System.out.println(">Inizializzo");

			ClusterModelMaker<LinearMatrix, Vector> clusterMaker = new VectorSimilarityClusterMaker<LinearMatrix, Vector>();
			lastString = "VSCM";

			AdvisableConsulter<LinearMatrix, Vector> advisableConsultor = new AdvisableConsulter<LinearMatrix, Vector>(
					clusterMaker, null, EXPLORATION_CONSTANT_ALPHA);
			CSVAdvisableRecordParser<Vector> csvVectorAdvisableRecordParser = new CSVAdvisableRecordParser<Vector>(
					files[FilesType.ITEMS.ordinal()], files[FilesType.ITEMS.ordinal()],
					fileUSERS, files[FilesType.PAYOFF.ordinal()]) {

				@Override
				public Advisable<Vector> parseAdvisable(String item) {
					String[] split = item.split(",");
					float[] parsed = new float[split.length];
					for (int i = 0; i < split.length; i++) {
						parsed[i] = Float.parseFloat(split[i]);
					}
					return new FeaturesAdvisable(parsed,item);

				}

				@Override
				public AdvisableRecord<Vector> generateRecord(String[] userID, String[] itemsToChoose,
						String[] correctAnswer, String[] payoff) {
					Cluster<Advisable<Vector>> availableVectors=new Cluster<Advisable<Vector>>();
					HashMap<Advisable<Vector>, Double> payoffs=new HashMap<Advisable<Vector>, Double>();
					int payoffIndex=0;
					float[] features = null;
					Integer featuresID=0;
					for (int i = 0; i < itemsToChoose.length; i++) {
						int featuresIndex = i%featuresSize;
						if (featuresIndex==0) {
							if (features!=null) {
								Advisable<Vector> item = new FeaturesAdvisable(features,featuresID.toString());
								featuresID++;
								availableVectors.add(item);
								payoffs.put(item, Double.parseDouble(payoff[payoffIndex++]));
							}
							features = new float[featuresSize];
						}
						features[featuresIndex]=Float.parseFloat(itemsToChoose[i]);
					}
					
					return new AdvisableRecord<Vector>(availableVectors, userID[0], payoffs);

				}

			};
			File fileGRAPH = files[FilesType.GRAPH.ordinal()];
			final HashMap<String, MoldableModelNode<LinearMatrix, Vector>> profileMap = new HashMap<String, MoldableModelNode<LinearMatrix, Vector>>();
			if (fileGRAPH!=null) {
				System.out.println(">CreoLeAdiacentLists");
	
				final int userFeaturesSize = featuresSize;
				List<String[]> adiacentList = CSVHandler.readCSV(fileGRAPH);
				
				MoldableModelNode<LinearMatrix, Vector> user = null;
				for (int i = 1; i <= 100; i++) {
					String userID=Integer.toString(i);
					user = new Seeker(userFeaturesSize, userID);
					profileMap.put(userID, user);
				}
				Iterator<String[]> adiacentListIterator = adiacentList.iterator();
				Integer userIntegerID=1;
				while (adiacentListIterator.hasNext()) {
					user = profileMap.get((userIntegerID++).toString());
					String[] neighborhood = (String[]) adiacentListIterator.next();
					for (int i = 0; i < neighborhood.length; i++) {
						MoldableModelNode<LinearMatrix, Vector> target = profileMap.get(neighborhood[i].replaceAll("\\D", ""));
						user.addDirectionalLink(new DirectionalLink(target,DEFAULT_WEIGHT));
					}
				}
			}
			

			ConsulterEvauator<LinearMatrix, Vector> consultorEvauator = new ConsulterEvauator<LinearMatrix, Vector>(
					advisableConsultor, csvVectorAdvisableRecordParser) {

				@Override
				public MoldableModel<LinearMatrix, Vector> getUser(String userID) {
					String clearedID = Integer.toString(Integer.parseInt(userID));
					MoldableModelNode<LinearMatrix, Vector> user = profileMap.get(clearedID);
					if (user == null) {
						System.out.println("Creo un utente");
						user = new Seeker(featuresSize,clearedID);
						profileMap.put(clearedID, user);
					}
					return user;
				}

			};
			consultorEvauator.setOnlyOnHit(onlyOnHit);
			System.out.println(">InizioIlTest");

			if (fileUSERS == null) {
				lastString += "_SINGLEUSER";
				consultorEvauator.testAndWriteAll(files[0].getParentFile().getName()+lastString+ EXPLORATION_CONSTANT_ALPHA);
			} else {
				consultorEvauator
						.testAndWriteAll(files[0].getParentFile().getName() + lastString+"_"+DEFAULT_WEIGHT+"wRELASIONS_NoC" + EXPLORATION_CONSTANT_ALPHA+"-"+advisableConsultor.PROPAGATE_MAX_DISTANCE+"-"+AdvisableConsulter.PROPAGATE_DISTANCE_PAYOFF_CONSERVATION);
			}

			System.out.println(">Fine");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
