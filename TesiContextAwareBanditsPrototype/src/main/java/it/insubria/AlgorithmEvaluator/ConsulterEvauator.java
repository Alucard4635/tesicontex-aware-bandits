package it.insubria.AlgorithmEvaluator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import it.insubria.cabAlgorithm.AdvisableConsulter;
import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.FuzzyEquals;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;

/**
 * @author Andrea Marisio<br>
 *this class is used for test anytype of AdvisableConsulter
 * @param <TypeOfMemory> type of learning system of the consulter
 * @param <TypeOfAdvisableRecord> type of adversing system
 */
public abstract class ConsulterEvauator<TypeOfMemory,TypeOfAdvisableRecord extends FuzzyEquals> {
	/**
	 * @author Andrea Marisio
	 * a simple state of a result
	 */
	public enum ResultTestState{MISSED,CORRECT_MISS,NEGATIVE_HIT,HIT}
	/**
	 * @author Andrea Marisio
	 * this small class is used for communicate meaning of results
	 */
	private class ResultTest{
		private ResultTestState state;
		private double value;

		public ResultTest(ResultTestState state, double value) {
			this.state = state;
			this.value = value;
			
		}
	}
	
	/**
	 * where pick up records
	 */
	private CSVAdvisableRecordParser<TypeOfAdvisableRecord> advisableRecordParser;
	/**
	 * the consulter under review
	 */
	private AdvisableConsulter<TypeOfMemory, TypeOfAdvisableRecord> consulterToTest;
	/**
	 * used for performance testing
	 */
	private static long[] oldMills=inizializateCurrentMillsArray(10);
	/**
	 * if you want to update the system only when have a known answer
	 */
	private boolean onlyOnHit = false;
	/**used for have multiple time for performance different things
	 * @param i number of times variables
	 * @return an array of long with the current time in mills
	 */
	public static long[] inizializateCurrentMillsArray(int i) {
		long[] ls = new long[i];
		for (int j = 0; j < ls.length; j++) {
			ls[j]=System.currentTimeMillis();
		}
		return ls;
	}
	/**
	 * @param consulterToTest consulter under review
	 * @param parser where pick up records
	 */
	public ConsulterEvauator(AdvisableConsulter<TypeOfMemory, TypeOfAdvisableRecord> consulterToTest,CSVAdvisableRecordParser<TypeOfAdvisableRecord> parser){
		 this.consulterToTest = consulterToTest;
		advisableRecordParser = parser;
	}
	/**
	 * @param record content to test
	 * @return a ResultTest get from an evaluation of the consulter
	 */
	public ResultTest testOnce(AdvisableRecord<TypeOfAdvisableRecord> record) {
		String userID = record.getUserToAdvice();
		MoldableModel<TypeOfMemory, TypeOfAdvisableRecord> user = getUser(userID);
		consulterToTest.setAdvisableSet(record.getWhatToAdvice());
		Advisable<TypeOfAdvisableRecord> itemToConsult = consulterToTest.consult(user);
		boolean hit=record.isAKnownAnswer(itemToConsult);
		Double payoff =record.getPayoff(itemToConsult);
		if (!isOnlyOnHit()||hit) { 
			consulterToTest.updateWeight(user, itemToConsult, (float)payoff.doubleValue());
		}
		if (hit) {
			if(payoff>0){
				return new ResultTest(ResultTestState.HIT, payoff);
			}else {
				return new ResultTest(ResultTestState.NEGATIVE_HIT, payoff);

			}
		}else {
			double payoffOnHit=record.getMaxPayoffOnHit();
			if (payoffOnHit<=0) {
				return new ResultTest(ResultTestState.CORRECT_MISS, payoff);

			}else {
				return new ResultTest(ResultTestState.MISSED, payoff);

			}
		}
	}
	/**this method is used for personalise where get users
	 * @param userID id of the {@link MoldableModel} element
	 * @return a {@link MoldableModel}
	 */
	public abstract MoldableModel<TypeOfMemory, TypeOfAdvisableRecord> getUser(String userID) ;
	
	/**run all test and write a file CSV with each row a state of the test, all together can make an accumulate diagram of efficiency
	 * @param nameFiles a string to add on the name of created statistics file
	 * @return number of lines, or test
	 * @throws IOException like on writing files, used on the writing of the results
	 */
	public int testAndWriteAll(String nameFiles) throws IOException{
		BufferedWriter writerStatistics=new BufferedWriter(new FileWriter(new File(nameFiles+"Statistics.csv")));

		int lines=0;
		ResultTestState[] values = ResultTestState.values();
		double[] resultsNumbers = new double[values.length+1];

		int ordinal;
		for (AdvisableRecord<TypeOfAdvisableRecord> advisableRecord : advisableRecordParser) {
			ResultTest resultTest = testOnce(advisableRecord);
			ResultTestState testOnceState = resultTest.state;
			ordinal = testOnceState.ordinal();
			lines++;
			resultsNumbers[ordinal]++;
			resultsNumbers[resultsNumbers.length-1]+=resultTest.value;
			for (int i = 0; i < resultsNumbers.length; i++) {
				writerStatistics.write(Double.toString(resultsNumbers[i]));
				if (i<resultsNumbers.length-1) {
					writerStatistics.write(",");
				}else {
					writerStatistics.write("\n");
				}
			}
			if (lines%1000==0) {
				System.out.println(lines+" "+testOnceState.toString());
			}
		}
		System.out.println(">Scrivo le statistiche");
		for (int i = 0; i < resultsNumbers.length; i++) {
			if (i<resultsNumbers.length-1) {
				writerStatistics.write(resultsNumbers[i]*100.0/lines+"%");
				writerStatistics.write(",");
			}else {
				writerStatistics.write(resultsNumbers[i]/lines+"");
			}
		}
		writerStatistics.flush();
		writerStatistics.close();
		return lines;
	}
	
	/**
	 * @return an array with the result of analysis order like the enumerative {@link ResultTestState}
	 * @throws IOException while reading records
	 */
	public int[] testAll() throws IOException{

		int lines=0;
		ResultTestState[] values = ResultTestState.values();
		int[] resultsNumbers = new int[values.length];

		int ordinal;
		List<AdvisableRecord<TypeOfAdvisableRecord>> records = advisableRecordParser.getRecords();
		for (AdvisableRecord<TypeOfAdvisableRecord> advisableRecord : records) {
			ResultTestState testOnce = testOnce(advisableRecord).state;
			ordinal = testOnce.ordinal();
			lines++;
			resultsNumbers[ordinal]++;
			System.out.println(lines+" "+testOnce.toString());
		}
		
		return resultsNumbers;
	}

	
	
	
	/**stamp delta time of a time variables
	 * @param before a string to stamp before the time for give him more meanings
	 * @param variable index of the variables, must be correct with the initialisation
	 */
	public static void checkMills(String before,int variable) {
		long currentTimeMillis = System.currentTimeMillis();
		if (before.length()>0) {
			System.out.println('>'+before);
		}
		System.out.println(" Time:"+(currentTimeMillis-oldMills[variable])+"ms");
		oldMills[variable] = currentTimeMillis;
	}
	/**
	 * @return true if the only on hit mode is active
	 */
	public boolean isOnlyOnHit() {
		return onlyOnHit;
	}
	/**
	 * @param onlyOnHit active or deactivate the only hit mode
	 */
	public void setOnlyOnHit(boolean onlyOnHit) {
		this.onlyOnHit = onlyOnHit;
	}

}
