package it.insubria.AlgorithmEvaluator;

import java.util.Collection;
import java.util.HashMap;

import it.insubria.cabAlgorithm.clusters.Cluster;
import it.insubria.cabAlgorithm.dataStructures.Advisable;
/**
 * @author Andrea Marisio<br>
 *
 * @param <ObjectType> type of advisable memory
 */
public class AdvisableRecord<ObjectType> {
	/**
	 * Relation between advisable and payoff of recommending it
	 */
	private HashMap<Advisable<ObjectType>, Double> payoff;
	/**
	 * a set of Advisable which consulter have to choose the best option
	 */
	private Cluster<Advisable<ObjectType>> whatToAdvice;
	/**
	 * user to consult
	 */
	private String userToAdviceID;
	
	/**
	 * @param setToSelect a set of Advisable which consulter have to choose the best option 
	 * @param whoInform user to consult
	 * @param payoffs Relation between advisable and payoff of recommending it
	 */
	public AdvisableRecord(Cluster<Advisable<ObjectType>>setToSelect,String whoInform,HashMap<Advisable<ObjectType>, Double> payoffs) {
		whatToAdvice = setToSelect;
		userToAdviceID = whoInform;
		this.payoff=payoffs;
	}
	

	/**
	 * @return the set of advisable of consultation
	 */
	public Cluster<Advisable<ObjectType>> getWhatToAdvice() {
		return whatToAdvice;
	}

	/**this method set the set of advisable of consultation
	 * @param whatToAdvice set of advisable of consultation
	 */
	public void setWhatToAdvice(Cluster<Advisable<ObjectType>> whatToAdvice) {
		this.whatToAdvice = whatToAdvice;
	}


	/**
	 * @return the identificator of the consulting user
	 */
	public String getUserToAdvice() {
		return userToAdviceID;
	}

	/**set identificator of the consulting user
	 * @param userToAdvice identificator of the consulting user
	 */
	public void setUserToAdvice(String userToAdvice) {
		this.userToAdviceID = userToAdvice;
	}

	/**Return a valuation of item, like if it was offered to the user of id insert on initialisation 
	 * @param itemToConsult item which want to offer to the ipotetic user
	 * @return the known payoff, 0.0 if it is unknown
	 */
	public Double getPayoff(Advisable<ObjectType> itemToConsult) {
		Double result = payoff.get(itemToConsult);
		if (result==null) {
			return 0.0;
		}
		return result;
	}
	
	/**check if a payoff is known for the advisable item
	 * with equals method and hashcode one
	 * @param item advisable to check
	 * @return true if present 
	 */
	public boolean isAKnownAnswer(Advisable<ObjectType> item){
		return payoff.containsKey(item);
	}

	/**
	 * @return the highest payoff known,<pre>{@code Double.NEGATIVE_INFINITY}</pre> if not present more
	 */
	public double getMaxPayoffOnHit() {
		Collection<Double> values = payoff.values();
		double result=Double.NEGATIVE_INFINITY;
		for (Double value: values) {
			result=Math.max(result, value.doubleValue());
		}
		return result;
	}
	
}
