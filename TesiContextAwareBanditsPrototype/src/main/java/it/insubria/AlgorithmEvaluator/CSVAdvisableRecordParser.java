package it.insubria.AlgorithmEvaluator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.insubria.cabAlgorithm.dataStructures.Advisable;

/**
 * @author Andrea Marisio<br>
 *this class read a CSV file and parse record like a subclass prefer
 * @param <TypeOfAdvisableRecord> type of advisable record generated
 */
public abstract class CSVAdvisableRecordParser<TypeOfAdvisableRecord> implements Iterable<AdvisableRecord<TypeOfAdvisableRecord>> {
	/**
	 * buffer for read users
	 */
	private BufferedReader users;
	/**
	 * buffer for read items
	 */
	private BufferedReader items;
	/**
	 * buffer for read correctAnswers
	 */
	private BufferedReader correctAnswers;
	/**
	 * buffer for read payoffs
	 */
	private BufferedReader payoffs;
	
	/**
	 * @param itemsCSV csv with advisable features
	 * @param correctAnswersCSV csv with answers
	 * @param usersCSV csv with users
	 * @param payoffsCSV csv with payoffs
	 * @throws FileNotFoundException 
	 */
	public CSVAdvisableRecordParser(File itemsCSV,File correctAnswersCSV,File usersCSV,File payoffsCSV) throws FileNotFoundException {
		if (usersCSV!=null) {
			this.users = new BufferedReader(new FileReader(usersCSV));
		}
		this.items = new BufferedReader(new FileReader(itemsCSV));
		this.correctAnswers = new BufferedReader(new FileReader(correctAnswersCSV));
		if (payoffsCSV!=null) {
			this.payoffs = new BufferedReader(new FileReader(payoffsCSV));
		}
		
	}
	/**
	 * @param itemsCSV csv with advisable features
	 * @param correctAnswersCSV csv with answers
	 * @param usersCSV csv with users
	 * @throws FileNotFoundException
	 */
	public CSVAdvisableRecordParser(File usersCSV,File itemsCSV,File correctAnswersCSV) throws FileNotFoundException {
		this(usersCSV, itemsCSV, correctAnswersCSV, null);
	}
	
	/**
	 * @return next record like an {@link Iterable}
	 * @throws IOException while reading files
	 */
	public AdvisableRecord<TypeOfAdvisableRecord> getRecord() throws IOException{
		String[] userIDs;
		if (users==null) {
			userIDs=new String[1];
			userIDs[0]="-1";
		}else {
			userIDs = users.readLine().split(",");
		}
		String[] itemsToChoose=items.readLine().split(",");
		String[] correctAnswer=correctAnswers.readLine().split(",");
		String[] payoff=payoffs==null?null:payoffs.readLine().split(",");
		return generateRecord( userIDs, itemsToChoose, correctAnswer, payoff);
	}
	
	/**
	 * @return true if all documents are ready,by ready() method
	 */
	public boolean canRead() {
		try {
			if (items.ready()&&correctAnswers.ready()) {
				if (users==null||users.ready()) {
					if (payoffs==null ||payoffs.ready()) {
						return true;
					}
				}
			}
		} catch (IOException e) {
			return false;
		}
		return false;
		
	}

	/**
	 * @return all record
	 * @throws IOException while reading
	 * @throws NumberFormatException if there are't numbers
	 */
	public List<AdvisableRecord<TypeOfAdvisableRecord>> getRecords() throws IOException, NumberFormatException{
		List<String[]> usersValues = CSVHandler.readAndCloseCSV(users);
		List<String[]> itemsValues = CSVHandler.readAndCloseCSV(items);
		List<String[]> correctAnswersValues = CSVHandler.readAndCloseCSV(correctAnswers);
		List<String[]> payoffsValues=null;
		if (payoffs!=null) {
			payoffsValues = CSVHandler.readAndCloseCSV(payoffs);
		}
		return generateRecords(usersValues,itemsValues,correctAnswersValues,payoffsValues);

	}
	/**
	 * @param usersValues lines of csv file
	 * @param itemsValues lines of csv file
	 * @param correctAnswersValues lines of csv file
	 * @param payoffsValues lines of csv file
	 * @return all records
	 */
	private List<AdvisableRecord<TypeOfAdvisableRecord>> generateRecords(
															List<String[]> usersValues, 
															List<String[]> itemsValues,
															List<String[]> correctAnswersValues, 
															List<String[]> payoffsValues) {
		
		Iterator<String[]> uIterator = usersValues.iterator();
		Iterator<String[]> iIterator=itemsValues.iterator();
		Iterator<String[]> cAIterator=correctAnswersValues.iterator();
		Iterator<String[]> pIterator=null;
		boolean noPayoff = payoffsValues!=null;
		if (noPayoff) {
			pIterator = payoffsValues.iterator();
		}

		LinkedList<AdvisableRecord<TypeOfAdvisableRecord>> result = new LinkedList<AdvisableRecord<TypeOfAdvisableRecord>>();
		for (  ; uIterator.hasNext()&&iIterator.hasNext()&&cAIterator.hasNext()&&(noPayoff||pIterator.hasNext());) {
			String[] userID = ((String[]) uIterator.next());
			String[] itemsToChoose = (String[]) iIterator.next();
			String[] correctAnswer = ((String[]) cAIterator.next());
			String[] payoff=null;
			if (noPayoff) {
				payoff = ((String[]) pIterator.next());
			}
			AdvisableRecord<TypeOfAdvisableRecord> advisableRecord = generateRecord( userID, itemsToChoose, correctAnswer,
					payoff);
			result.add(advisableRecord);
		}
		return result;
	}
	/**
	 * @param userID values of a users CSV line
	 * @param itemsToChoose values of a itemsToChoose CSV line
	 * @param correctAnswer values of a correctAnswer CSV line
	 * @param payoff values of a payoff CSV line
	 * @return a record
	 */
	public abstract AdvisableRecord<TypeOfAdvisableRecord> generateRecord( String[] userID, String[] itemsToChoose,
			String[] correctAnswer, String[] payoff) ;
	
		
	/**
	 * @param item a row of an advisable 
	 * @return An advisable
	 */
	public abstract Advisable<TypeOfAdvisableRecord> parseAdvisable(String item) ;
	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator<AdvisableRecord<TypeOfAdvisableRecord>> iterator() {
		
		return new Iterator<AdvisableRecord<TypeOfAdvisableRecord>>() {

			public boolean hasNext() {
				return canRead();
			}

			public AdvisableRecord<TypeOfAdvisableRecord> next() {
					try {
						return getRecord();
					} catch (IOException e) {
						return null;
					}

			}
		};
	}
}
