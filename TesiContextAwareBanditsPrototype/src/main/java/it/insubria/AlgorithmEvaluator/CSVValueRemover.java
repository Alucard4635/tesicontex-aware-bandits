package it.insubria.AlgorithmEvaluator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class CSVValueRemover {
public static void main(String[] args) {
	JFileChooser f=new JFileChooser();
	f.setCurrentDirectory(new File(System.getProperty("user.dir")));
	File mainFile;
	do {
		JOptionPane.showMessageDialog(null, "Select file that need to be clean");
		int retVal = f.showOpenDialog(null);
		if (retVal!=JFileChooser.APPROVE_OPTION) {
			break;
		}else {
			mainFile = f.getSelectedFile();
		}
		String showInputDialog = JOptionPane.showInputDialog("insert values to remove", "value1,value2");
		if (showInputDialog.length()<=0) {
			break;
		}
		String[] toSkip = showInputDialog.split(",");
		HashSet<String> valuesLessToSkip=new HashSet<String>();
		for (int i = 0; i < toSkip.length; i++) {
			valuesLessToSkip.add(toSkip[i]);
		}
		
		boolean startWithANewline=false;
		System.out.println("rippo "+mainFile);
		try {
			BufferedWriter writer=new BufferedWriter(new FileWriter(new File(mainFile.getName()+"Removed"+showInputDialog+".csv")));
			BufferedReader reader = new BufferedReader(new FileReader(mainFile));
			Stream<String> lines = reader.lines();
			Iterator<String> iterator = lines.iterator();
			int currentLineNumber=0;
			while (iterator.hasNext()) {
				String[] value = ((String) iterator.next()).split(",");
				CSVHandler.writeAValueSet(writer, valuesLessToSkip, value, startWithANewline);
				startWithANewline=true;
				if (currentLineNumber++%1000==0) {
					writer.flush();
					System.out.println(currentLineNumber-1);
				}
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} while (true);
}
}
