package it.insubria.AlgorithmEvaluator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * @author Andrea Marisio<br>
 *write statistic and ripping some values
 */
public abstract class StatisticDataRipper {

	private static final String DELIMITER = ",";







	/**
	 * 
	 */
	public StatisticDataRipper() {
	}

	/**
	 * @param fileToAnalize analyse this fule
	 * @param percentage percentage of occurrence for analyse if ripped on that threeshold
	 * @return the remaining lines after ripping
	 * @throws IOException on read
	 */
	public static HashSet<String> analize(File fileToAnalize,double percentage) throws IOException {
		
		List<String[]> readedCSV = CSVHandler.readCSV(fileToAnalize);
		HashMap<String, Integer> valueCount = countWord(readedCSV);
		return writeStatistics(fileToAnalize, valueCount,percentage);
	}

	/**
	 * @param readedCSV list of values of a CSV
	 * @return a map that indicate the occurrence of keys
	 */
	private static HashMap<String, Integer> countWord(List<String[]> readedCSV) {
		Integer oldValue;
		HashMap<String, Integer> valueCount = new HashMap<String, Integer>();
		for (String[] strings : readedCSV) {
			for (int i = 0; i < strings.length; i++) {
				oldValue = valueCount.get(strings[i]);
				if (oldValue==null) {
					oldValue = 0;
				}
				valueCount.put(strings[i], oldValue + 1);
			}
		}
		return valueCount;
	}
	


	/** sort a map
	 * @param unsortMap 
	 * @param order
	 * @return
	 */
	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {
		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());
		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<String, Integer>>() {
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				if (order) {
					return o1.getValue().compareTo(o2.getValue());
				} else {
					return o2.getValue().compareTo(o1.getValue());
				}
			}
		});
		// Maintaining insertion order with the help of LinkedList
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Entry<String, Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	/**
	 * @param linesToSkipUnorded
	 * @param fileWithDependencies
	 * @param valuesLessToSkip
	 * @param stringToAddOnFilesNames
	 * @param valueDelimiter 
	 * @throws IOException
	 */
	public static void ripFiles(ArrayList<Integer> linesToSkipUnorded, File[] fileWithDependencies, HashSet[] valuesLessToSkip,
		String stringToAddOnFilesNames, String valueDelimiter) throws IOException {
		File folder=new File("RippedFiles");
		folder.mkdir();
		BufferedWriter writer;
		
		List<String[]> content;
		Integer currentLine;
//		int size;
		@SuppressWarnings("unchecked")
		ArrayList<Integer> linesToSkip=(ArrayList<Integer>) linesToSkipUnorded.clone();
		
		Collections.sort(linesToSkip, new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2) {
					return o1.compareTo(o2);
				}});
		for (int f = 0; f < fileWithDependencies.length; f++) {
			File file=fileWithDependencies[f];
			writer=new BufferedWriter(new FileWriter(folder.getPath()+File.separator+file.getName()+stringToAddOnFilesNames+".csv"));
			//read a file with iterator
			Iterator<String[]> iterator = new CSVHandler(file, valueDelimiter);
			
			currentLine=0;
			boolean skip;
			int index=0;
			boolean alreadyWrote=false;
			while (iterator.hasNext()) {
				String[] values=iterator.next();
				
				skip=false;
				while (linesToSkip.size()>index&&linesToSkip.get(index).intValue()==currentLine) {
					skip=true;
					index++;
				}
				if(!skip){
					CSVHandler.writeAValueSet(writer, valuesLessToSkip[f], values, alreadyWrote);
					alreadyWrote=true;
				}
					currentLine++;
			}
			
//			content=CSVHandler.readCSV(file);
//				currentLine=0;
//				boolean skip;
//				int index=0;
//				boolean alreadyWrote=false;
//				for (String[] values : content) {
//					skip=false;
//					while (linesToSkip.size()>index&&linesToSkip.get(index).intValue()==currentLine) {
//						skip=true;
//						index++;
//					}
//					if(!skip){
//						CSVHandler.writeAValueSet(writer, valuesLessToSkip[f], values, alreadyWrote);
//						alreadyWrote=true;
//					}
//					currentLine++;
//				}
			writer.flush();
			writer.close();
		}
	
}

	/**write statistics on file look
	 * @param file
	 * @param valueCount
	 * @param percentageUnder
	 * @return
	 * @throws IOException
	 */
	private static HashSet<String> writeStatistics(File file, HashMap<String, Integer> valueCount,double percentageUnder) throws IOException {
		
		Map<String, Integer> sortByValues = sortByComparator(valueCount,true);
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(file.getName()+"Statistics"+percentageUnder*100+"%.csv")));
		int total = sortByValues.size();
		int totalValues=0;

		Integer value;
		for (Entry<String, Integer> entry : sortByValues.entrySet()) {
			value = entry.getValue();
			totalValues+=value;
			bufferedWriter.write("Key : " + entry.getKey() + " Count : " + value+"\n");
		}
		int average=totalValues/total;
		bufferedWriter.write("\n");
		bufferedWriter.write("Total Elements: "+total+"\n");
		bufferedWriter.write("Total Lines: "+totalValues+"\n");
		bufferedWriter.write("Average: "+average+"\n");
		int underTheAverage = 0;
		int lineUnderTheAverage = 0;
		int lineUnderHalfAverage = 0;
		int underHalfAverage = 0;
		int currentSumOfValues = 0;
		int threeshold = -1;
		int underThreeshold=0;
		Integer linesUnderThreeshold = 0;
		HashSet<String> valuesUnderTheTreshold=new HashSet<String>();
		for (Entry<String, Integer> entry : sortByValues.entrySet()) {
			value = entry.getValue();
			currentSumOfValues+=value;
			if (currentSumOfValues/(double)totalValues<percentageUnder) {
				underThreeshold++;
				linesUnderThreeshold += value;
				threeshold = value.intValue();
				valuesUnderTheTreshold.add(entry.getKey());
			}
			if (value<average/2) {
				lineUnderHalfAverage += value;
				underHalfAverage++;
			}
			if (value<average) {
				lineUnderTheAverage += value;
				underTheAverage++;
			}else {
				if (currentSumOfValues/(double)totalValues>percentageUnder){
					break;
				}
			}
		}
		
		int remaningLines;

		
		remaningLines = totalValues-linesUnderThreeshold;
		bufferedWriter.write("Threeshold Found for ("+percentageUnder*100+"%): "+threeshold+"\n");
		bufferedWriter.write("Elements Under The Threeshold: "+underThreeshold+" ("+underThreeshold*100.0/(double)total+"%)\n");
		bufferedWriter.write("Elements if ripped on Threeshold: "+(total-underThreeshold)+" ("+(100-(underThreeshold*100.0/(double)total))+"%)\n");
		bufferedWriter.write("Lines if ripped on Threeshold: "+remaningLines+" ("+remaningLines*100.0/(double)totalValues+"%)\n");
		bufferedWriter.write("\n");
		remaningLines = totalValues-lineUnderTheAverage;
		bufferedWriter.write("Elements Under The Average: "+underTheAverage+" ("+underTheAverage*100.0/(double)total+"%)\n");
		bufferedWriter.write("Elements if ripped on Average: "+(total-underTheAverage)+" ("+(100-(underTheAverage*100.0/(double)total))+"%)\n");
		bufferedWriter.write("Lines if ripped on  Average: "+remaningLines+" ("+remaningLines*100.0/(double)totalValues+"%)\n");
		remaningLines = totalValues-lineUnderHalfAverage;
		bufferedWriter.write("Elements Under Half Average: "+underHalfAverage+" ("+underHalfAverage*100.0/(double)total+"%)\n");
		bufferedWriter.write("Elements if ripped on Half Average: "+(total-underHalfAverage)+" ("+(100-(underHalfAverage*100.0/(double)total))+"%)\n");
		bufferedWriter.write("Lines if ripped on Half Average: "+remaningLines+" ("+remaningLines*100.0/(double)totalValues+"%)");
		bufferedWriter.flush();
		bufferedWriter.close();
		return valuesUnderTheTreshold;
	}

	/**
	 * @param map
	 * @param decreasingOrder
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	private static HashMap sortByValues(HashMap map, final boolean decreasingOrder) {
       List list = new LinkedList(map.entrySet());
       // Defined Custom Comparator here
       Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
            	if (decreasingOrder) {
            		return ((Comparable) ((Map.Entry) (o1)).getValue())
                            .compareTo(((Map.Entry) (o2)).getValue());
				}else {
					return ((Comparable) ((Map.Entry) (o2)).getValue())
			                  .compareTo(((Map.Entry) (o1)).getValue());
				}
            }
       });

       // Here I am copying the sorted list in HashMap
       // using LinkedHashMap to preserve the insertion order
       HashMap sortedHashMap = new LinkedHashMap();
       for (Iterator it = list.iterator(); it.hasNext();) {
              Map.Entry entry = (Map.Entry) it.next();
              sortedHashMap.put(entry.getKey(), entry.getValue());
       } 
       return sortedHashMap;
	  }
	
	

	
	


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFileChooser f=new JFileChooser();
		f.setMultiSelectionEnabled(true);
		f.setCurrentDirectory(new File(System.getProperty("user.dir")));

		JOptionPane.showMessageDialog(null, "Select files which removes lines");
		int retVal = f.showOpenDialog(null);
		HashSet<String>[] valuesLessUsedMainFileOrder = null;
		ArrayList<Integer> linesToSkip = null;
		File[] mainFiles;
		File[] fileWithDependencies;
		File[] filesWhichBelong;
		if (retVal==JFileChooser.APPROVE_OPTION) {
			
			mainFiles = f.getSelectedFiles();
			JOptionPane.showMessageDialog(null, "Select files which have lines dependencies");
			retVal = f.showOpenDialog(null);
			if (retVal!=JFileChooser.APPROVE_OPTION) {
				System.exit(0);
			}
			fileWithDependencies = f.getSelectedFiles();
			f.setMultiSelectionEnabled(false);
			filesWhichBelong=new File[fileWithDependencies.length];
			for (int i = 0; i < fileWithDependencies.length; i++) {
				JOptionPane.showMessageDialog(null, "Select file which thier content belong for "+fileWithDependencies[i].getName());
				retVal = f.showOpenDialog(null);
				if (retVal==JFileChooser.APPROVE_OPTION) {
					filesWhichBelong[i]=f.getSelectedFile();
				}
			}
			
			String showInputDialog = JOptionPane.showInputDialog("Insert % of threeshold");
			double percentage=-1;
			String endingString;
			boolean isAPercentage = showInputDialog.endsWith("%");
			if (isAPercentage) {
				percentage = Double.parseDouble(showInputDialog.substring(0, showInputDialog.length()-1))/100;
			}else {
				percentage=Double.parseDouble(showInputDialog);
			}
			
			
			String stringToAddOnFilesNames = "RippedBy";
			for (int i = 0; i < mainFiles.length; i++) {
				stringToAddOnFilesNames+=mainFiles[i].getName();
			}
			endingString = stringToAddOnFilesNames+"On"+percentage*100+"%";
			
			valuesLessUsedMainFileOrder=new HashSet[mainFiles.length];
			for (int i = 0; i < mainFiles.length; i++) {
				try {
					valuesLessUsedMainFileOrder[i] = StatisticDataRipper.analize(mainFiles[i], percentage);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			try {
				System.out.println("Calcolo Le linee da saltare a "+percentage);
				linesToSkip = CSVHandler.getLinesWithThatValues(mainFiles, valuesLessUsedMainFileOrder);
				System.out.println("Trovo i valori sotto la soglia"+percentage);

				
				HashSet[] valuesLessToSkip = new HashSet[fileWithDependencies.length+mainFiles.length];
				for (int j = 0; j < mainFiles.length; j++) {
					for (int i = 0; i < fileWithDependencies.length; i++) {
						if (mainFiles[j].equals(filesWhichBelong[i])) {
							valuesLessToSkip[i+mainFiles.length]=valuesLessUsedMainFileOrder[j];
						}
					}
				}
				
				File[] allFiles = new File[fileWithDependencies.length+mainFiles.length];
				for (int i = 0; i < allFiles.length; i++) {
					if (i<mainFiles.length) {
						allFiles[i]=mainFiles[i];
					}else {
						allFiles[i]=fileWithDependencies[i-mainFiles.length];
					}
				}
				System.out.println("Rippo "+percentage);
				StatisticDataRipper.ripFiles(linesToSkip, allFiles,valuesLessToSkip, endingString, DELIMITER);
				System.out.println("Fine");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		
		
	}

}
