package it.insubria.AlgorithmEvaluator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * @author Andrea Marisio<br>
 * remap value of CSV files and files with dipendence of it
 */
public class CSVValueRemapper {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFileChooser f=new JFileChooser();
		f.setCurrentDirectory(new File(System.getProperty("user.dir")));
		File mainFile;
		do {
			JOptionPane.showMessageDialog(null, "Select file that need to be remapped");
			int retVal = f.showOpenDialog(null);
			if (retVal!=JFileChooser.APPROVE_OPTION) {
				break;
			}else {
				mainFile = f.getSelectedFile();
			}
			JOptionPane.showMessageDialog(null, "Select files which have value dependencies on main file");
			retVal = f.showOpenDialog(null);
			File fileWithDipendencies = null;
			if (retVal==JFileChooser.APPROVE_OPTION) {
				fileWithDipendencies = f.getSelectedFile();
			}
			BufferedReader mainBufferedReader = null;
			try {
				mainBufferedReader = new BufferedReader(new FileReader(mainFile));
			HashMap<String, String> oldToNewValueMap=CSVHandler.remapValues(mainBufferedReader);
			CSVHandler.createARemappedCSV(oldToNewValueMap,mainFile,fileWithDipendencies);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		} while (true);

	}
}
