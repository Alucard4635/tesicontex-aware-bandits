package it.insubria.AlgorithmEvaluator;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import it.insubria.cabAlgorithm.AdvisableConsulter;
import it.insubria.cabAlgorithm.clusters.Cluster;
import it.insubria.cabAlgorithm.clusters.ClusterModelMaker;
import it.insubria.cabAlgorithm.clusters.SimplifiedMultiClusterMoldableModelManager;
import it.insubria.cabAlgorithm.clusters.VectorSimilarityClusterMaker;
import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.DiagonalMatrix;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.OnAxisAdvisable;
import it.insubria.cabAlgorithm.dataStructures.OnAxisVector;
import it.insubria.cabAlgorithm.dataStructures.SimplifiedSeeker;

/**
 * @author Andrea Marisio<br>
 * this class is used for read CSV files and test consultor
 */
public class MainNoFeatures {
	/**
	 * used exploration parameter for consulter
	 */
	private static final double EXPLORATION_CONSTANT_ALPHA = 0.1;

	/**
	 * @author Andrea Marisio
	 */
	private static enum FilesType {
		ITEMS, KNOWN_ANSWERS, USERS, PAYOFF
	}

	/**
	 * @param args classic args of main
	 */
	public static void main(String[] args) {
		FilesType[] values = FilesType.values();
		File[] files = new File[values.length];
		JFileChooser f = new JFileChooser();
		f.setCurrentDirectory(new File(System.getProperty("user.dir")));
		for (int i = 0; i < files.length; i++) {
			JOptionPane.showMessageDialog(null, "Select " + values[i] + " csv file");
			int retVal = f.showOpenDialog(null);
			if (retVal != JFileChooser.APPROVE_OPTION && i != FilesType.PAYOFF.ordinal()
					&& i != FilesType.USERS.ordinal()) {
				System.exit(0);
			} else {
				if (retVal == JFileChooser.APPROVE_OPTION) {
					files[i] = f.getSelectedFile();
				}
			}
		}

		boolean onlyOnHit = false;
		if (files[FilesType.PAYOFF.ordinal()] != null) {
			onlyOnHit = true;
			System.out.println("HIT_ONLY");
		}

		System.out.println(">Scansiono Tutti gli oggetti");
		int maxValue = 0;
		try {

			Collection<Integer> allPossibleIntegerValues = CSVHandler
					.getAllPossibleIntegerValues(files[FilesType.ITEMS.ordinal()]).values();
			Cluster<Advisable<OnAxisVector>> iSet = new Cluster<Advisable<OnAxisVector>>();
			for (Integer integer : allPossibleIntegerValues) {
				int intValue = integer.intValue();
				iSet.add(new OnAxisAdvisable(intValue));
				if (intValue > maxValue) {
					maxValue = intValue;
				}
			}

			System.out.println("MaxItemsValue=" + maxValue + " of " + allPossibleIntegerValues.size());
			System.out.println(">Scansiono Tutti gli utenti");
			String lastString = "";
			if (files[FilesType.USERS.ordinal()] != null) {

				Collection<Integer> allPossibleIntegerValuesUser = CSVHandler
						.getAllPossibleIntegerValues(files[FilesType.USERS.ordinal()]).values();
				Cluster<MoldableModel<DiagonalMatrix, OnAxisVector>> biggerOne = new Cluster<MoldableModel<DiagonalMatrix, OnAxisVector>>();
				for (Integer integer : allPossibleIntegerValuesUser) {
					biggerOne.add(new SimplifiedSeeker(maxValue + 1, integer.toString()));
				}
				System.out.println("NumberOfUsers=" + biggerOne.size());
			}

			System.out.println(">Inizializzo");
//			 ClusterModelMaker<DiagonalMatrix, OnAxisVector> clusterMaker=new
//			 SimplifiedMultiClusterMoldableModelManager(
//			 allPossibleIntegerValues.size());lastString="SMCMMM";

			// ClusterModelMaker<DiagonalMatrix, OnAxisVector> clusterMaker=new
			// MultiClusterModableModelManager<DiagonalMatrix, OnAxisVector>(
			// allPossibleIntegerValues.size());lastString="MCMMM";
			 
			ClusterModelMaker<DiagonalMatrix, OnAxisVector> clusterMaker = new VectorSimilarityClusterMaker<DiagonalMatrix, OnAxisVector>();
			lastString = "VSCM";

			AdvisableConsulter<DiagonalMatrix, OnAxisVector> advisableConsultor = new AdvisableConsulter<DiagonalMatrix, OnAxisVector>(
					clusterMaker, iSet, EXPLORATION_CONSTANT_ALPHA);

			CSVAdvisableRecordParser<OnAxisVector> csvOnAxisVectorAdvisableRecordParser = new CSVAdvisableRecordParser<OnAxisVector>(
					files[FilesType.ITEMS.ordinal()], files[FilesType.KNOWN_ANSWERS.ordinal()],
					files[FilesType.USERS.ordinal()], files[FilesType.PAYOFF.ordinal()]) {

				@Override
				public Advisable<OnAxisVector> parseAdvisable(String item) {
					return new OnAxisAdvisable(Integer.parseInt(item));

				}

				@Override
				public AdvisableRecord<OnAxisVector> generateRecord(String[] userIDs, String[] itemsToChoose,
						String[] correctAnswer, String[] payoff) {
					Cluster<Advisable<OnAxisVector>> setToSelect = new Cluster<Advisable<OnAxisVector>>();
					for (int i = 0; i < itemsToChoose.length; i++) {
						setToSelect.add(new OnAxisAdvisable(Integer.parseInt(itemsToChoose[i])));
					}
					HashMap<Advisable<OnAxisVector>, Double> payoffs = new HashMap<Advisable<OnAxisVector>, Double>();
					for (int i = 0; i < correctAnswer.length; i++) {
						double payoffForThisLog=1;
						if (payoff!=null) {
							payoffForThisLog = Double.parseDouble(payoff[i]);
						}
						payoffs.put(new OnAxisAdvisable(Integer.parseInt(correctAnswer[i])),
								payoffForThisLog);
					}
					AdvisableRecord<OnAxisVector> result = new AdvisableRecord<OnAxisVector>(setToSelect, userIDs[0],
							payoffs);
					return result;
				}

			};

			final int userFeaturesSize = maxValue + 1;
			ConsulterEvauator<DiagonalMatrix, OnAxisVector> consultorEvauator = new ConsulterEvauator<DiagonalMatrix, OnAxisVector>(
					advisableConsultor, csvOnAxisVectorAdvisableRecordParser) {

				private HashMap<String, MoldableModel<DiagonalMatrix, OnAxisVector>> profileMap = new HashMap<String, MoldableModel<DiagonalMatrix, OnAxisVector>>();

				@Override
				public MoldableModel<DiagonalMatrix, OnAxisVector> getUser(String userID) {
					MoldableModel<DiagonalMatrix, OnAxisVector> user = profileMap.get(userID);
					if (user == null) {
						user = new SimplifiedSeeker(userFeaturesSize, userID);
						profileMap.put(userID, user);
					}
					return user;
				}

			};
			consultorEvauator.setOnlyOnHit(onlyOnHit);
			System.out.println(">InizioIlTest");
			lastString+= "C="+AdvisableConsulter.CLUSTER_AFFIDABILTY_COEFFICIENT;
			if (files[FilesType.USERS.ordinal()] == null) {
				lastString += "_SINGLEUSER";
				consultorEvauator.testAndWriteAll(files[0].getParentFile().getName()+lastString);
			} else {
				consultorEvauator
						.testAndWriteAll(files[0].getParentFile().getName() + lastString + EXPLORATION_CONSTANT_ALPHA);
			}

			System.out.println(">Fine");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
