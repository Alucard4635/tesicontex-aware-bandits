package it.insubria.socialNetwork;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Andrea Marisio<br>
 * a focus in a graph
 */
public class Focus implements Serializable {
	/**
	 * for deserialization
	 */
	private static final long serialVersionUID = -8555153057871406532L;
	/**
	 * 
	 */
	private String focusId;
	
	/**
	 * @param id ID
	 */
	public Focus(String id) {
		focusId = id;
	}
	
	/**
	 * for deseralization
	 */
	private Focus() {
	}
	
	/**
	 * data structure of who points this edge
	 */
	private ConcurrentHashMap<String, AbstractNode> nodes=new ConcurrentHashMap<String, AbstractNode>();

	/**
	 * @param abstractNode a node to enroll on this focus
	 */
	public void enroll(AbstractNode abstractNode) {
		nodes.put(abstractNode.getId(), abstractNode);
	}

	/**
	 * @return enroll nodes
	 */
	public ConcurrentHashMap<String, AbstractNode> getNodes() {
		return nodes;
	}
	
	/**
	 * @return average number of closed triangles on this focus 
	 */
	public double getFocusClousure(){
		double hit=0.0;
		Collection<AbstractNode> myNodes = nodes.values();
		if (myNodes.isEmpty()) {
			return 0.0;
		}
		for (AbstractNode node : myNodes) {
			for (AbstractNode target : myNodes) {
				if (node.isAdiacentNode(target)) {
					hit++;
				}
			}
		}
		return (hit/myNodes.size())/myNodes.size();
	}
	
	/**
	 * @return average number of closed triangles on this focus 
	 */
	public double getMembershipClousure(){
		double hit=0.0;
		Collection<AbstractNode> myNodes = nodes.values();
		for (AbstractNode node : myNodes) {
			hit += getMembershipClousure(node);
		}
		return hit;
	}

	/**
	 * @param node node which calculate membership clousure
	 * @return average number of closed triangles on this focus for this node
	 */
	public double getMembershipClousure(AbstractNode node) {
		double hit=0.0;
		Collection<DirectionalLink> adiacencyList = node.getAdiacencyList();
		if(adiacencyList.isEmpty()){
			return 0;
		}
		
		for (DirectionalLink targetlink : adiacencyList) {
			String targetID = targetlink.getTarget().getId();
			if (nodes.get(targetID)!=null) {
				hit++;
			}
		}
		return hit/adiacencyList.size();
	}

	/**
	 * @return ID of this focus
	 */
	public String getId() {
		return focusId;
	}

	/**
	 * @param focusId ID to set
	 */
	public void setId(String focusId) {
		this.focusId = focusId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return focusId.hashCode();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return focusId;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Focus ) {
			return focusId.equals(((Focus) obj).focusId);
		}
		return super.equals(obj);
	}
}
