package it.insubria.socialNetwork;

import java.io.Serializable;

/**
 * @author Andrea Marisio<br>
 *represent a link in a graph
 */
public class DirectionalLink implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6242562391728284448L;
	/**
	 * 
	 */
	private static Integer UID=new Integer(0);
	/**
	 * 
	 */
	private Integer incrementalID=getNewIncrementalID();
	/**
	 * @return
	 */
	public synchronized Integer getNewIncrementalID() {
		return UID++;
	}
	/**
	 * 
	 */
	protected AbstractNode targetNode;
	/**
	 * 
	 */
	private double weight=1;
	/**
	 * @param target node pointed by this link
	 */
	public DirectionalLink(AbstractNode target){
		this(target,0.0);
	}
	/**
	 * @param abstractNode node pointed by this link
	 * @param weight weight of this edge
	 */
	public DirectionalLink(AbstractNode abstractNode,double weight){
		this.targetNode = abstractNode;
		this.weight = weight;
	}
	/**
	 * @return node pointed by this link
	 */
	public AbstractNode getTarget() {
		return targetNode;
	}
	/**
	 * @param target node pointed by this link
	 */
	public void setTarget(AbstractNode target) {
		this.targetNode = target;
	}
	/**
	 * @return weight of this link
	 */
	public double getWeight() {
		return weight;
	}
	/**
	 * @param weight weight of this link
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DirectionalLink) {
			DirectionalLink link = (DirectionalLink) obj;
			if (targetNode.equals(link.targetNode)&&weight==link.weight) {
				return true;
			}
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "N: "+targetNode+" W: "+weight;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return incrementalID.hashCode();
	}
}
