package it.insubria.socialNetwork;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Andrea Marisio<br>
 * a class that contains nodes and focus
 */
public class Graph implements Serializable {
	/**
	 * for deserialization
	 */
	private static final long serialVersionUID = -6152011564808931861L;
	/**
	 * data structure of nodes
	 */
	private ConcurrentHashMap<String, AbstractNode> nodes;
	/**
	 * data structure of focus
	 */
	private ConcurrentHashMap<String, Focus> focus;

	/**
	 * 
	 */
	public Graph() {
		setNodes(new ConcurrentHashMap<String, AbstractNode>());
		setFocus(new ConcurrentHashMap<String, Focus>());

	}

	/**
	 * @param nodeCapacity nodes initial capacity for better performance
	 * @param focusCapacity focus initial capacity for better performance
	 */
	public Graph(int nodeCapacity, int focusCapacity) {
		setNodes(new ConcurrentHashMap<String, AbstractNode>(nodeCapacity));
		setFocus(new ConcurrentHashMap<String, Focus>(focusCapacity));
	}

	/**get a node , null if don't exits
	 * @param id id of node to find
	 * @return the instance in this graph
	 */
	public AbstractNode getNode(String id) {
		AbstractNode requiredNode = takeNode(id);
		return requiredNode;
	}

	/**get a Focus , null if don't exits
	 * @param id id of Focus to find
	 * @return the instance in this graph
	 */
	public Focus getFocus(String id) {
		Focus requiredFocus = takeFocus(id);
		
		return requiredFocus;
	}

	/**
	 * @param id
	 * @return
	 */
	private Focus takeFocus(String id) {
		return focus.get(id);
	}

	/**remove focus
	 * @param id id of focus to remove
	 * @return removed focus, null otherwise
	 */
	public Focus removeFocus(String id) {
		return focus.remove(id);
	}

	/**remove node
	 * @param id id of node to remove
	 * @return removed node, null otherwise
	 */
	public AbstractNode removeNode(String id) {
		return nodes.remove(id);
	}

	/**
	 * @param id
	 * @return
	 */
	private AbstractNode takeNode(String id) {
		return nodes.get(id);
	}

	/**
	 * @return a list of the instances node in this graph
	 */
	public Collection<AbstractNode> getNodes() {
		return nodes.values();
	}

	/**calculate the distance of this nodes
	 * @param n1 starting node
	 * @param n2 node to find
	 * @return the distance, -1 if disconnected
	 */
	public static int distance(Node n1, Node n2) {
		LinkedList<AbstractNode> visited = new LinkedList<AbstractNode>();
		LinkedList<AbstractNode> toVisit = new LinkedList<AbstractNode>();
		toVisit.add(n1);
		visited.add(n1);
		int distance = 0;
		int nodeCurrentDistance = 1;
		AbstractNode currentNode;
		Collection<DirectionalLink> adiacency;
		while (!toVisit.isEmpty()) {
			currentNode = toVisit.pop();

			nodeCurrentDistance--;// System.out.println(currentNode+": "+nodeCurrentDistance+" "+distance);
			if (currentNode.equals(n2)) {
				return distance;
			}

			adiacency = currentNode.getAdiacencyList();
			for (DirectionalLink node : adiacency) {
				AbstractNode target = node.getTarget();
				if (!visited.contains(target)) {
					visited.add(target);
					toVisit.add(target);
				}
			}
			if (nodeCurrentDistance == 0) {
				distance++;
				nodeCurrentDistance = toVisit.size();
			}

		}
		return -1;
	}

	/**set the map of this nodes
	 * @param nodes map of this nodes to set
	 */
	public void setNodes(ConcurrentHashMap<String, AbstractNode> nodes) {
		this.nodes = nodes;
	}

	/**
	 * @return collection of reference of this focuses
	 */
	public Collection<Focus> getFocus() {
		return focus.values();
	}

	/**set the map of this focuses
	 * @param focus map of this focus to set
	 */
	public void setFocus(ConcurrentHashMap<String, Focus> focus) {
		this.focus = focus;
	}


}
