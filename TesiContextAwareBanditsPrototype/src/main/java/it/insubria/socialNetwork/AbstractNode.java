package it.insubria.socialNetwork;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Andrea Marisio<br>
 * A node in a graph
 */
public abstract class AbstractNode implements Serializable {
	/**
	 * Id for a correct Deserialization
	 */
	private static final long serialVersionUID = -35711535441145908L;
	/**
	 * an id user for hashCode and fast identification
	 */
	private String id;
	/**
	 * number of incoming edges
	 */
	private int inDegree = 0;
	/**
	 * number of outcoming edges
	 */
	private int outDegree = 0;

	/**
	 * @param idendt
	 *            an ID
	 */
	public AbstractNode(String idendt) {
		id = idendt;
	}

	/**
	 * a constructor for serialization
	 */
	private AbstractNode() {
	}

	/**
	 * connect another AbstractNode using a DirectLink, this one have got a
	 * reference of an AbstractNode this type of connection is bidirectional, by
	 * invoking
	 * 
	 * <pre>
		* {@code link.getTarget().directionalLink(this,link.getWeight());
		* }
	 * </pre>
	 * 
	 * @param link edge to connect
	 */
	public void link(DirectionalLink link) {
		directionalLink(link);
		link.targetNode.directionalLink(this,link.getWeight());
	}

	/**connect another AbstractNode using a DirectLink, this one have got a
	 * reference of an AbstractNode this type of connection is directional
	 * @param link  edge to connect
	 */
	public void directionalLink(DirectionalLink link) {
		AbstractNode target = link.getTarget();
		if (target != null && !target.equals(this)) {
			updateDegree(target);
			addDirectionalLink(link);
		}
	}

	/**connect another AbstractNode using a DirectLink, this one have got a
	 * reference of an AbstractNode this type of connection is directional, and initialize a default directLink
	 * @param target AbstractNode pointed by edge
	 * @param weight weight of edge
	 */
	public void directionalLink(AbstractNode target, double weight) {
		directionalLink(new DirectionalLink(target, weight));
	}

	/**add the link to adeguate data structure
	 * @param link link  edge to connect
	 */
	protected abstract void addDirectionalLink(DirectionalLink link);

	/**update degre of a directional link
	 * @param target AbstractNode pointed by edge
	 */
	private void updateDegree(AbstractNode target) {
		target.inDegree++;
		outDegree++;
	}

	/**
	 * * connect another AbstractNode using a DirectLink, this one have got a
	 * reference of an AbstractNode this type of connection is bidirectional,initialize a default directLink and
	 * invoke
	 * 
	 * <pre>
		* {@code link.getTarget().directionalLink(link);
		* }
	 * </pre>
	 * 
	 * @param target AbstractNode pointed by edge
	 * @param weight weight of edge
	 */
	public void link(AbstractNode target, double weight) {
		directionalLink(target, weight);
		target.directionalLink(this, weight);
	}

	/**
	 * @return neighborhood
	 */
	public abstract Collection<DirectionalLink> getAdiacencyList();

	/**set AbstractNode's neighborhood
	 * @param list neighborhood to set
	 */
	public abstract void setAdiacencyList(Collection<DirectionalLink> list);

	/**check in the neighborhood if target exist
	 * @param target node to find
	 * @return <pre> {@code findAdiacentNode(target.getId())!=null}</pre>
	 */
	public boolean isAdiacentNode(AbstractNode target) {
		AbstractNode node = findAdiacentNode(target.getId());
		return node != null;
	}

	/**
	 * @param targetID id of searched node
	 * @return finded node
	 */
	public abstract AbstractNode findAdiacentNode(String targetID);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (id == null) {
			return -1;
		}
		return id.hashCode();
	}

	/**
	 * @return node's identifier 
	 */
	public String getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AbstractNode) {
			AbstractNode n = (AbstractNode) obj;
			if (this.id == null) {
				return null == n.id;
			} else {

				return this.id.equals(n.id);
			}

		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	/**
	 * @return number of incoming edges
	 */
	public int getInDegree() {
		return inDegree;
	}

	/**
	 * @return number of outcoming edges
	 */
	public int getOutDegree() {
		return outDegree;
	}

	/**
	 * @return average number of triangles closed for neighboring
	 */
	public double triadricClosure() {
		Collection<DirectionalLink> list = getAdiacencyList();
		if (list.isEmpty()) {

			return 0;
		}

		double hit = 0;
		// boolean gotHit;
		// double numberOfFriendWithATriangle=0.0;
		double closedTriangles;
		for (DirectionalLink directionalLink : list) {
			AbstractNode target = directionalLink.getTarget();
			closedTriangles = getTrianglesClosed(target);

			if (closedTriangles > 0) {
				hit += closedTriangles;
				// numberOfFriendWithATriangle++;
			}
		}
		return hit / list.size();
	}

	/**
	 * @param target neighboring whitch look for triangles
	 * @return average number of triangles closed for neighboring
	 */
	private double getTrianglesClosed(AbstractNode target) {
		Collection<DirectionalLink> listOfMyTarget = target.getAdiacencyList();
		if (listOfMyTarget.isEmpty()) {
			return 0;
		}
		double hit = 0;
		for (DirectionalLink targetOfMyTarget : listOfMyTarget) {
			if (isAdiacentNode(targetOfMyTarget.getTarget())) {
				hit++;
			}
		}

		return hit / listOfMyTarget.size();
	}

}
