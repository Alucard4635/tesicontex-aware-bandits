package it.insubria.socialNetwork;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

/**
 * @author Andrea Marisio<br>
 * a simple node implemented with a list
 */
public class Node extends AbstractNode implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 874058324452008749L;
	/**
	 * 
	 */
	private Collection<DirectionalLink> adiacencyList;
	
	
	/**
	 * @param idendt an ID
	 */
	public Node(String idendt) {
		super(idendt);
		adiacencyList=new LinkedList<DirectionalLink>();
	}
	
	

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#findAdiacentNode(java.lang.String)
	 */
	public AbstractNode findAdiacentNode(String targetID) {
		for (DirectionalLink directionalLink : adiacencyList) {
			AbstractNode target = directionalLink.getTarget();
			if (target.getId().equals(targetID)) {
				return target;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#getAdiacencyList()
	 */
	public Collection<DirectionalLink> getAdiacencyList() {
		return adiacencyList;
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#addDirectionalLink(it.insubria.socialNetwork.DirectionalLink)
	 */
	@Override
	protected void addDirectionalLink(DirectionalLink link) {
		adiacencyList.add(link);
		
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#setAdiacencyList(java.util.Collection)
	 */
	@Override
	public void setAdiacencyList(Collection<DirectionalLink> list) {
		adiacencyList = new LinkedList<DirectionalLink>(list);
		
	}

}
