package it.insubria.socialNetwork;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Andrea Marisio<br>
 *a node with an adjacency list made with hashmap
 */
public class HashNode extends AbstractNode implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1701272504994237935L;
	/**
	 * 
	 */
	private ConcurrentHashMap<String,DirectionalLink> adiacencyMap;
	
	
	/**
	 * @param idendt ID
	 */
	public HashNode(String idendt) {
		super(idendt);
		adiacencyMap=new ConcurrentHashMap<String,DirectionalLink>();
	}
	
	
	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#isAdiacentNode(it.insubria.socialNetwork.AbstractNode)
	 */
	@Override
	public boolean isAdiacentNode(AbstractNode target) {
		return adiacencyMap.containsKey(target.getId());
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#findAdiacentNode(java.lang.String)
	 */
	public AbstractNode findAdiacentNode(String targetID) {
		DirectionalLink link = adiacencyMap.get(targetID);
		AbstractNode target = null;
		if (link!=null) {
			target = link.getTarget();
		}
		return target ;
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#getAdiacencyList()
	 */
	public Collection<DirectionalLink> getAdiacencyList() {
		return adiacencyMap.values();
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#addDirectionalLink(it.insubria.socialNetwork.DirectionalLink)
	 */
	@Override
	protected void addDirectionalLink(DirectionalLink link) {
		String targetID=link.getTarget().getId();
		adiacencyMap.put(targetID,link);
		
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#setAdiacencyList(java.util.Collection)
	 */
	@Override
	public void setAdiacencyList(Collection<DirectionalLink> list) {
		adiacencyMap=new ConcurrentHashMap<String,DirectionalLink>();
		for (DirectionalLink directionalLink : list) {
			addDirectionalLink(directionalLink);
		}
		
	}

}
