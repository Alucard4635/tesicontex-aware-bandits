package it.insubria.cabAlgorithm.clusters;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Set;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;

/**
 * @author Andrea Marisio <br>
 * This prototype is an approximation of CAB algorithm
 *         witch make clusters and then manage them without recreate all
 * @param <Memory> type of learning system of models 
 * @param <Model> type of advisable system of models
 */
public class MultiClusterMoldableModelManager<Memory, Model> implements ClusterModelMaker<Memory, Model> {
	/**
	 * data structure which manage all cluster
	 */
	private HashMap<Advisable<Model>, HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>>> ClusterMap;
	/**
	 * exploration parameter
	 */
	private double esplorationParameter=Double.MIN_VALUE;

	/**
	 * @param sartingAdvisableSize starting sizes for improve performance
	 */
	public MultiClusterMoldableModelManager(int sartingAdvisableSize) {

		ClusterMap = new HashMap<Advisable<Model>, HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>>>(
				sartingAdvisableSize);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.insubria.cabAlgorithm.clusters.ClusterModelMaker#getSubCluster(it.
	 * insubria.cabAlgorithm.dataStructures.MoldableModel,
	 * it.insubria.cabAlgorithm.dataStructures.Advisable, double)
	 */
	public ClusterMoldableModel<Memory, Model> getSubCluster(MoldableModel<Memory, Model> aUser,
			Advisable<Model> anItem, double esplorationParameter) {
		this.esplorationParameter = esplorationParameter;
		HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>> itemMap = ClusterMap
				.get(anItem);
		if (itemMap == null) {// if the items clusters don't exists make
								// it//FIXME it is too slow
			HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>> added;
			added = addAnAdvisableClusterGroup(anItem);
			assignToHisCluster(aUser, anItem);
			return added.get(aUser);
		}
		ClusterMoldableModel<Memory, Model> clusterMoldableModel = itemMap.get(aUser);
		if (clusterMoldableModel == null) {
			update(aUser);
			clusterMoldableModel = itemMap.get(aUser);
		}

		return clusterMoldableModel;
	}

	/**create all clusters on for the new item, or replace it if present
	 * @param anItem which create items
	 * @return created map
	 */
	public HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>> addAnAdvisableClusterGroup(
			Advisable<Model> anItem) {
		HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>> map;
		map = new HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>>();
		if (ClusterMap.isEmpty()) {
			ClusterMap.put(anItem, map);
			return map;
		}
		Set<MoldableModel<Memory, Model>> usersUpdated = ClusterMap.values().iterator().next().keySet();

		ClusterMap.put(anItem, map);
		for (MoldableModel<Memory, Model> model : usersUpdated) {
			assignToHisCluster(model, anItem);
		}
		return map;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.insubria.cabAlgorithm.clusters.ClusterModelMaker#update(it.insubria.
	 * cabAlgorithm.dataStructures.MoldableModel,
	 * it.insubria.cabAlgorithm.dataStructures.Advisable[])
	 */
	public void update(MoldableModel<Memory, Model> aUser, Advisable<Model>... unused) {

		Set<Advisable<Model>> values = ClusterMap.keySet();
		for (Advisable<Model> advisable : values) {
			assignToHisCluster(aUser, advisable);
		}			
	}

	/**assign a model in a compatible cluster
	 * @param aUser model to insert
	 * @param advisable an advisable for identify cluster group
	 */
	public void assignToHisCluster(MoldableModel<Memory, Model> aUser, Advisable<Model> advisable) {
		ClusterMoldableModel<Memory, Model> hisCluster;
		Collection<ClusterMoldableModel<Memory, Model>> otherClusters;
		ClusterMoldableModel<Memory, Model> selectedNewCluster;
		HashMap<MoldableModel<Memory, Model>, ClusterMoldableModel<Memory, Model>> itemMap;

		itemMap = ClusterMap.get(advisable);
		hisCluster = itemMap.get(aUser);

		selectedNewCluster = null;
		if (hisCluster != null) {
			// if
			// (hisCluster.canAdd(aUser,advisable,esplorationParameter)&&hisCluster.size()>1)
			// {
			// return;
			// }else {
			hisCluster.remove(aUser);
			itemMap.remove(aUser);
			// }
		}

		otherClusters = itemMap.values();
		selectedNewCluster = selectNewClusterFor(otherClusters, aUser, advisable, esplorationParameter);
		if (selectedNewCluster == null) {
			selectedNewCluster = new SimplifiedCABClusterMoldableModel<Memory, Model>(aUser);
		} else {
			selectedNewCluster.add(aUser);
		}
		itemMap.put(aUser, selectedNewCluster);
	}

	/**
	 * @param otherClusters
	 * @param aUser
	 * @param advisable
	 * @param explorPar
	 * @return
	 */
	private ClusterMoldableModel<Memory, Model> selectNewClusterFor(
			Collection<ClusterMoldableModel<Memory, Model>> otherClusters, MoldableModel<Memory, Model> aUser,
			Advisable<Model> advisable, double explorPar) {
		return getFirstAceptable(otherClusters, aUser, advisable, explorPar);

	}

	/**
	 * @param otherClusters
	 * @param aUser
	 * @param advisable
	 * @param explorPar
	 * @return the first cluster that respect the getMebership role
	 */
	public ClusterMoldableModel<Memory, Model> getFirstAceptable(
			Collection<ClusterMoldableModel<Memory, Model>> otherClusters, MoldableModel<Memory, Model> aUser,
			Advisable<Model> advisable, double explorPar) {
		for (ClusterMoldableModel<Memory, Model> clusterToCheck : otherClusters) {
			if (clusterToCheck.getMembership(aUser, advisable, explorPar) >= 0) {
				return clusterToCheck;
			}
		}
		return null;
	}

}
