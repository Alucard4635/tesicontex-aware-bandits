package it.insubria.cabAlgorithm.clusters;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;

/**
 * @author Andrea Marisio<br>
 *
 * @param <ContentType> type of content's learning system 
 * @param <ModelClassType>  type of content's advisable system 
 */
public interface ClusterMaker<ContentType,ModelClassType> {
	/**create a cluster based on an element is contained
	 * @param content the element which create a cluster
	 * @param features item which create a cluster
	 * @param esplorationParameter 
	 * @return a cluster based on features on input
	 */
	public Cluster<ContentType> getSubCluster(ContentType content,Advisable<ModelClassType> features,double esplorationParameter);






}
