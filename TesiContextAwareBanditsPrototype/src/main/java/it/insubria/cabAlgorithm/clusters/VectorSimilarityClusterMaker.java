package it.insubria.cabAlgorithm.clusters;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.FuzzyEquals;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;

/**
 * @author Andrea Marisio<br>
 *a class that make cluster like CAB Algorithm
 * @param <MemoryType> learning system of models
 * @param <ModelClassType> advisable system of models
 */
public class VectorSimilarityClusterMaker<MemoryType, ModelClassType extends FuzzyEquals>
		implements ClusterModelMaker<MemoryType, ModelClassType> {
	/**
	 * the cluster with all model to make clusters
	 */
	private Cluster<MoldableModel<MemoryType, ModelClassType>> baseCluster = new Cluster<MoldableModel<MemoryType, ModelClassType>>();

	/**
	 * starting with an empty cluster, the users are added on update
	 */
	public VectorSimilarityClusterMaker() {
	}
	
	/**
	 * starting with a baseCluster cluster, the users are added/updated on update
	 */
	public VectorSimilarityClusterMaker( Cluster<MoldableModel<MemoryType, ModelClassType>> baseCluster) {
		this.baseCluster = baseCluster;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterModelMaker#getSubCluster(it.insubria.cabAlgorithm.dataStructures.MoldableModel, it.insubria.cabAlgorithm.dataStructures.Advisable, double)
	 */
	@Override
	public ClusterMoldableModel<MemoryType, ModelClassType> getSubCluster(
			MoldableModel<MemoryType, ModelClassType> profile, Advisable<ModelClassType> whatToAdvice,
			double updatedEsplorationParameter) {
		final double dotProductOfThisProfile = profile.getModel().getFuzzyCoefficient(whatToAdvice.features());
		final double userConfidenceBound = updatedEsplorationParameter * profile.getConfidenceBound(whatToAdvice);
		ClusterMoldableModel<MemoryType, ModelClassType> result = new ClusterMoldableModel<MemoryType, ModelClassType>(profile) {
			@Override
			public double getMembership(MoldableModel<MemoryType, ModelClassType> aUser, Advisable<ModelClassType> advisable,
					double explorPar) {
				double similarity = Math.abs(dotProductOfThisProfile
								- aUser.getModel().getFuzzyCoefficient(advisable.features()));
				double CB = userConfidenceBound
								+ explorPar * aUser.getConfidenceBound(advisable);
				return CB-similarity ;
			}

			@Override
			protected ClusterMoldableModel<MemoryType, ModelClassType> clone() throws CloneNotSupportedException {return null;}
		};
		
		for (MoldableModel<MemoryType, ModelClassType> seeker : baseCluster) {
			double membership = result.getMembership(seeker, whatToAdvice, updatedEsplorationParameter);
			if ((!profile.equals(seeker)) && membership>=0) {
				result.add(seeker);
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterModelMaker#update(it.insubria.cabAlgorithm.dataStructures.MoldableModel, it.insubria.cabAlgorithm.dataStructures.Advisable[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void update(final MoldableModel<MemoryType, ModelClassType> aUser,
			final Advisable<ModelClassType>... unused) {
		baseCluster.update(aUser);
	}

}
