package it.insubria.cabAlgorithm.clusters;

import java.util.HashMap;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.DiagonalMatrix;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.OnAxisVector;

/**
 * @author Andrea Marisio<br>
 * this class create a cluster that implements getMembership with the CAB clustering algorithm
 * @param <MemoryType> type of learning system of clusters
 * @param <AdvisableModelClassType> type of advisable system of clusters
 */
public class SimplifiedCABClusterMoldableModel<MemoryType, AdvisableModelClassType> extends ClusterMoldableModel<MemoryType, AdvisableModelClassType> {
	
	/**
	 * @param model which start the cluster
	 */
	public SimplifiedCABClusterMoldableModel(MoldableModel<MemoryType, AdvisableModelClassType> model) {
		super(model);
	}


	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterMoldableModel#getMembership(it.insubria.cabAlgorithm.dataStructures.MoldableModel, it.insubria.cabAlgorithm.dataStructures.Advisable, double)
	 */
	@Override
	public double getMembership(MoldableModel<MemoryType, AdvisableModelClassType> aUser,
			Advisable<AdvisableModelClassType> advisable, double explorPar) 
	{
		double dotProductOfThisSeeker =aUser.getModel().getFuzzyCoefficient(advisable.features());
		double similarity = Math.abs(
				dotProductOfThisSeeker
				- getAUser().getModel().getFuzzyCoefficient( advisable.features())
				);
		double explotation = explorPar*(getAUser().getConfidenceBound(advisable)+aUser.getConfidenceBound(advisable));
		return  explotation- similarity ;
	}


	/**
	 * @return a representative  model
	 */
	public MoldableModel<MemoryType, AdvisableModelClassType> getAUser() {
		/*if (size()>0) {
			return iterator().next();
		}*/
		return clusterModel;
	}


	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterMoldableModel#clone()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected SimplifiedCABClusterMoldableModel<MemoryType, AdvisableModelClassType> clone() throws CloneNotSupportedException {
		SimplifiedCABClusterMoldableModel<MemoryType, AdvisableModelClassType> clone = new SimplifiedCABClusterMoldableModel<>(this.clusterModel);
		clone.content=(HashMap<Integer, MoldableModel<MemoryType, AdvisableModelClassType>>) content.clone();
		return clone;
	}


	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterMoldableModel#addAll(it.insubria.cabAlgorithm.clusters.ClusterMoldableModel)
	 */
	public void addAll(ClusterMoldableModel<MemoryType, AdvisableModelClassType> clusterToAdd) {
		super.addAll(clusterToAdd);
	}
}
