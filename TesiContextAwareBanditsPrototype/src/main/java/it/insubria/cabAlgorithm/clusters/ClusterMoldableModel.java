package it.insubria.cabAlgorithm.clusters;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.DiagonalMatrix;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.OnAxisVector;

/**
 * @author Andrea Marisio<br>
 *A cluster with a overall profile, which is updated every time a compatible model is added/removed/updated
 * @param <MemoryType>  type of learning system of the overall model
 * @param <AdvisableModelClassType>  type of advisable system of the overall model
 */
public abstract class ClusterMoldableModel<MemoryType,AdvisableModelClassType> extends Cluster<MoldableModel<MemoryType,AdvisableModelClassType>> {
	/**
	 * overall profile, which is updated every time a compatible model is added/removed/updated
	 */
	protected MoldableModel<MemoryType,AdvisableModelClassType> clusterModel;
	
	/**
	 * @param model the starting model in the cluster,this model is added on cluster
	 */
	public ClusterMoldableModel(MoldableModel<MemoryType,AdvisableModelClassType> model) {
		super.add(model);
		setClusterModel(model);
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.Cluster#add(java.lang.Object)
	 */
	@Override
	public MoldableModel<MemoryType, AdvisableModelClassType> add(MoldableModel<MemoryType,AdvisableModelClassType> element) {
		MoldableModel<MemoryType, AdvisableModelClassType> moldableModel = super.update(element);
		/*if (moldableModel!=null) {
			clusterModel.subtractMemory(moldableModel);
		}*/
		clusterModel.addMemory(element);

		return moldableModel;
	}
	
	
	/**This method return an a value that measure the compatibility of aUser,
	 * if the returning value is &lt;&#61;0 it means that element is compatible, otherwise it's not
	 * @param aUser the model to evaluate
	 * @param advisable which item measure the confidential value
	 * @param explorPar the flexibility of this evaluation
	 * @return a value that measure the confidence with cluster content
	 */
	public abstract double getMembership(
			MoldableModel<MemoryType,AdvisableModelClassType> aUser,
			Advisable<AdvisableModelClassType> advisable, double explorPar);


	/**
	 * @return the overall model
	 */
	public MoldableModel<MemoryType,AdvisableModelClassType> getClusterModel() {
		return clusterModel;
	}
	/**
	 * @param clusterModel the model that have to represent this cluster, the value is passed by deep copy
	 */
	public void setClusterModel(MoldableModel<MemoryType,AdvisableModelClassType> clusterModel) {
		try {
			this.clusterModel = clusterModel.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.Cluster#clone()
	 */
	protected abstract ClusterMoldableModel<MemoryType,AdvisableModelClassType> clone() throws CloneNotSupportedException;
	
	/**join this cluster with another
	 * @param clusterToAdd cluster to join
	 */
	public void addAll(ClusterMoldableModel<MemoryType, AdvisableModelClassType> clusterToAdd) {
		clusterModel.addMemory(clusterToAdd.clusterModel);
		super.addAll(clusterToAdd);
	}
	
	@Override
	public MoldableModel<MemoryType, AdvisableModelClassType> update(
			MoldableModel<MemoryType, AdvisableModelClassType> element) {
		MoldableModel<MemoryType, AdvisableModelClassType> moldableModel = super.update(element);
		if (moldableModel!=null) {
			clusterModel.subtractMemory(moldableModel);
		}
		clusterModel.addMemory(element);
		return moldableModel;
	}
}