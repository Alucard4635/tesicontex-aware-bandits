package it.insubria.cabAlgorithm.clusters;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.DiagonalMatrix;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.OnAxisVector;

/**
 * @author Andrea Marisio<br>
  * This prototype is an approximation of CAB algorithm, and an optimisation of {@link MultiClusterMoldableModelManager}
 *         witch make clusters and then manage them without recreate all
 */
public class SimplifiedMultiClusterMoldableModelManager implements ClusterModelMaker<DiagonalMatrix, OnAxisVector> {
	/**
	 * data structure which manage all cluster
	 */
	private HashMap<Advisable<OnAxisVector>, HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>> clusterMap;
	
	private HashMap<Advisable<OnAxisVector>, HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>>> removeMap=
			new HashMap<Advisable<OnAxisVector>,HashMap<MoldableModel<DiagonalMatrix,OnAxisVector>, Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>>>();
	/**
	 * exploration parameter
	 */
	private double esplorationParameter=Double.MIN_VALUE;
	
	/**
	 * @param sartingAdvisableSize starting sizes for improve performance
	 */
	public SimplifiedMultiClusterMoldableModelManager(int sartingAdvisableSize) {
		
		clusterMap=new HashMap<Advisable<OnAxisVector>,
				HashMap<MoldableModel<DiagonalMatrix,OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>>(sartingAdvisableSize);
		
	}
	
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterModelMaker#getSubCluster(it.insubria.cabAlgorithm.dataStructures.MoldableModel, it.insubria.cabAlgorithm.dataStructures.Advisable, double)
	 */
	@Override
	public ClusterMoldableModel<DiagonalMatrix, OnAxisVector> getSubCluster(
			MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector> anItem,
			double esplorationParameter) {
		this.esplorationParameter = esplorationParameter;
		HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> itemMap = clusterMap.get(anItem);
		if (itemMap==null) {//if the items clusters don't exists make it//FIXME it is too slow
			HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> added;
			added = addAnAdvisableClusterGroup( anItem);
			assignToHisCluster(aUser, anItem);
			ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel = added.get(aUser);
			return clusterMoldableModel;
		}else {
			ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel = itemMap.get(aUser);
			if (clusterMoldableModel==null) {
				update(aUser,anItem);
				clusterMoldableModel = itemMap.get(aUser);
			}
			return clusterMoldableModel;
		}
	}

	/**create all clusters(initially there is only one cluster, because none was referred it) on for the new item, or replace it if present
	 * @param anItem which create items
	 * @return created map
	 */
	public HashMap<MoldableModel<DiagonalMatrix,OnAxisVector>,ClusterMoldableModel<DiagonalMatrix,OnAxisVector>> addAnAdvisableClusterGroup(
			Advisable<OnAxisVector> anItem) {
		HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> map;
		map = new HashMap<MoldableModel<DiagonalMatrix,OnAxisVector>, ClusterMoldableModel<DiagonalMatrix,OnAxisVector>>();
		clusterMap.put(anItem,map );
		return map;
		
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterModelMaker#update(it.insubria.cabAlgorithm.dataStructures.MoldableModel, it.insubria.cabAlgorithm.dataStructures.Advisable[])
	 */
	@Override
	public void update(MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector>... used) {
		
		for (int i = 0; i < used.length; i++) {
			Advisable<OnAxisVector> advisable = used[i];
			assignToHisCluster(aUser, advisable);
		}
	}

	/**assign a model in a compatible cluster
	 * @param aUser model to insert
	 * @param advisable an advisable for identify cluster group
	 */
	public void assignToHisCluster(MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector> advisable) {
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> hisCluster;
		Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> otherClusters;
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> selectedNewCluster;
		HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> itemMap;
		
		itemMap=clusterMap.get(advisable);
		hisCluster = itemMap.get(aUser);
		
		selectedNewCluster=null;
		if (hisCluster!=null) {
			removeFromClusters(aUser, hisCluster, itemMap);
//			forJoinCluster
//			HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>> itemRemoveMap = removeMap.get(advisable);
//			if (itemRemoveMap==null) {
//				itemRemoveMap= new HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>>();
//				removeMap.put(advisable, itemRemoveMap);
//			}
//			Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> removeCollection = itemRemoveMap.get(aUser);
//			for (ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel : removeCollection) {
//				clusterMoldableModel.remove(aUser);
//			}
//			itemMap.remove(aUser);
			
		}
		
		otherClusters = itemMap.values();
		selectedNewCluster = addToANewCluster(aUser, advisable, otherClusters);
		itemMap.put(aUser, selectedNewCluster);
	}

	private ClusterMoldableModel<DiagonalMatrix, OnAxisVector> addToANewCluster(
			MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector> advisable,
			Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> otherClusters) {
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> selectedNewCluster;
		selectedNewCluster = selectNewClusterFor(otherClusters,aUser,advisable,esplorationParameter);
		if (selectedNewCluster==null) {
			selectedNewCluster=new  SimplifiedCABClusterMoldableModel<DiagonalMatrix, OnAxisVector>(aUser);
		}else {
			selectedNewCluster.add(aUser);
		}
		return selectedNewCluster;
	}

	/**
	 * @param aUser user to remove
	 * @param hisCluster cluster where remove a user
	 * @param itemMap where remove user
	 */
	private void removeFromClusters(MoldableModel<DiagonalMatrix, OnAxisVector> aUser,
			ClusterMoldableModel<DiagonalMatrix, OnAxisVector> hisCluster, 
			HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> itemMap) {
		
		hisCluster.remove(aUser);
		itemMap.remove(aUser);
	}

	/**
	 * @param otherClusters
	 * @param aUser
	 * @param advisable
	 * @param explorPar
	 * @return
	 */
	private ClusterMoldableModel<DiagonalMatrix,OnAxisVector> selectNewClusterFor(
			Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> otherClusters,
			MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector> advisable, double explorPar) {
		return getFirstAceptable(otherClusters, aUser, advisable, explorPar);
		
	}
	
	
//	/**
//	 * @param otherClusters
//	 * @param aUser
//	 * @param advisable
//	 * @param explorPar
//	 * @return the best cluster that respect the getMebership role, max value
//	 */
//	public ClusterMoldableModel<DiagonalMatrix, OnAxisVector> getJoinCluster(
//			Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> otherClusters,
//			MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector> advisable, double explorPar) {
//		double bestValue = 0;
//		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> result = null;
//		Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> list=new LinkedList<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>();
//		for (ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterToCheck : otherClusters) {
//			double canAdd = clusterToCheck.getMembership(aUser,advisable,explorPar);
//			if (bestValue>=0) {
//				if (result==null) {
//					result=new SimplifiedCABClusterMoldableModel<DiagonalMatrix, OnAxisVector>(aUser);
//					result.remove(aUser);//i don't want to modify other code
//				}
//				result.addAll(clusterToCheck);
//				list.add(clusterToCheck);
//			}
//		}
//		for (ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel : list) {
//			clusterMoldableModel.add(aUser);
//		}
//		HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>> itemRemoveMap = removeMap.get(advisable);
//		if (itemRemoveMap==null) {
//			itemRemoveMap= new HashMap<MoldableModel<DiagonalMatrix, OnAxisVector>, Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>>>();
//			removeMap.put(advisable, itemRemoveMap);
//			}
//		Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> removeCollection = itemRemoveMap.get(aUser);
//		if (removeCollection!=null) {
//			removeCollection.addAll(list);
//		}else {
//			itemRemoveMap.put(aUser, list);
//		}
//		return result;
//	}
	

	/**
	 * @param otherClusters
	 * @param aUser
	 * @param advisable
	 * @param explorPar
	 * @return the best cluster that respect the getMebership role, max value
	 */
	public ClusterMoldableModel<DiagonalMatrix, OnAxisVector> getBestCluster(
			Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> otherClusters,
			MoldableModel<DiagonalMatrix, OnAxisVector> aUser, Advisable<OnAxisVector> advisable, double explorPar) {
		double bestValue = 0;
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> result = null;
		for (ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterToCheck : otherClusters) {
			double canAdd = clusterToCheck.getMembership(aUser,advisable,explorPar);
			if (bestValue>=canAdd) {
				bestValue=canAdd;
				result= clusterToCheck;
			}
		}
		return result;
	}
	/**
	 * @param otherClusters
	 * @param aUser
	 * @param advisable
	 * @param explorPar
	 * @return the first cluster that respect the getMebership role
	 */
	public ClusterMoldableModel<DiagonalMatrix, OnAxisVector> getFirstAceptable(
			Collection<ClusterMoldableModel<DiagonalMatrix, OnAxisVector>> otherClusters, MoldableModel<DiagonalMatrix, OnAxisVector> aUser,
			Advisable<OnAxisVector> advisable, double explorPar) {
		for (ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterToCheck : otherClusters) {
			if (clusterToCheck.getMembership(aUser,advisable,explorPar)>=0) {
				return clusterToCheck;
			}
		}
		return null;
	}




}
