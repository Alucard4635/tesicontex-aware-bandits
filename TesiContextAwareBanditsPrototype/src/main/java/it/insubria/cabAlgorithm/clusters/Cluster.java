package it.insubria.cabAlgorithm.clusters;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author Andrea Marisio<br>
 *  An aggregation system based on an hashMap
 * @param <T>
 */
public class Cluster<T> implements Iterable<T>, Cloneable {
	/**
	 * an hashmap used like an hashset for use the hashmap method, and because i
	 * store item not "key"(used for a correct meaning)
	 */
	protected HashMap<Integer, T> content = new HashMap<Integer, T>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator<T> iterator() {
		return content.values().iterator();
	}

	/**Add or substitute if present
	 * @param element item to add
	 * @return the old element, null if absent
	 */
	public T add(T element) {
		return content.put(element.hashCode(), element);
	}

	/**Remove if present,it's hashCode and equals method indicate presence
	 * @param element the element to remove
	 * @return
	 */
	public boolean remove(T element) {
		return content.remove(element.hashCode()) == null;
	}

	/**
	 * @return number of element
	 */
	public int size() {
		return content.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return content.toString();
	}

	/**update the element,add it if not present
	 * @param element which update
	 * @return the substituted value, null 
	 */
	public T update(T element) {
		return content.put(element.hashCode(), element);
	}

	/**used for obtain the element content in this cluster, null if not present
	 * used for see an element which {@code e.equals(cluster.get(e))!=true}
	 * @param element 
	 * @return the element content in this cluster, null if not present
	 */
	public T get(T element) {
		return content.get(element.hashCode());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cluster) {
			return content.equals(((Cluster) obj).content);
		}
		return super.equals(obj);
	}

	/**
	 * @return the first element on this cluster
	 */
	public T getFirst() {
		Iterator<T> iterator = content.values().iterator();
		if (iterator.hasNext()) {
			return iterator.next();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Cluster<T> clone() throws CloneNotSupportedException {
		Cluster<T> clone = new Cluster<T>();
		clone.content = (HashMap<Integer, T>) content.clone();
		return clone;
	}

	/**join this cluster with another
	 * @param clusterToAdd cluster to join
	 */
	public void addAll(Cluster<T> clusterToAdd) {
		content.putAll(clusterToAdd.content);
	}
}
