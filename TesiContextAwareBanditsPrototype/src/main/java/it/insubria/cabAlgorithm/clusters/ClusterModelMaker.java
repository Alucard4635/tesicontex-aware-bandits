package it.insubria.cabAlgorithm.clusters;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;

/**
 * @author Andrea Marisio<br>
 *a cluster with an overall model
 * @param <MemoryClassType>
 * @param <ModelClassType>
 */
public interface ClusterModelMaker<MemoryClassType, ModelClassType> extends ClusterMaker<MoldableModel<MemoryClassType, ModelClassType>,ModelClassType> {
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.clusters.ClusterMaker#getSubCluster(java.lang.Object, it.insubria.cabAlgorithm.dataStructures.Advisable, double)
	 */
	public ClusterMoldableModel<MemoryClassType,ModelClassType> getSubCluster(
			MoldableModel<MemoryClassType, ModelClassType> aUser,
			Advisable<ModelClassType> anItem, double esplorationParameter);
	/**update the state of an user, if the user has changed you have to call this method
	 * @param aUser model to update
	 * @param lastOffers optional item which has contributed the update
	 */
	public void update(MoldableModel<MemoryClassType, ModelClassType> aUser,Advisable<ModelClassType>... lastOffers);
	

}
