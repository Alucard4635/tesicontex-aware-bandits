package it.insubria.cabAlgorithm;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import it.insubria.cabAlgorithm.clusters.Cluster;
import it.insubria.cabAlgorithm.clusters.ClusterMaker;
import it.insubria.cabAlgorithm.clusters.ClusterModelMaker;
import it.insubria.cabAlgorithm.clusters.ClusterMoldableModel;
import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.MoldableModelNode;
import it.insubria.socialNetwork.AbstractNode;
import it.insubria.socialNetwork.DirectionalLink;
import it.insubria.cabAlgorithm.dataStructures.FuzzyEquals;

/**
 * @author Andrea Marisio<br>
 * A consulter of advisable items implemented wit CAB Algorithm
 * @param <MemoryClassType> class used for memorise profile models, look {@link MoldableModel}
 * @param <ModelClassType> class used for the adversing items,look {@link Advisable} and {@link MoldableModel}
 */
public class AdvisableConsulter<MemoryClassType, ModelClassType extends FuzzyEquals> {

	/**
	 * a propagation constant for reduce payoff by distance like 
	 * {@code Math.pow(PROPAGATE_DISTANCE_PAYOFF_CONSERVATION, distance)}
	 */
	public static final double PROPAGATE_DISTANCE_PAYOFF_CONSERVATION = 0.7;
	public static final double CLUSTER_AFFIDABILTY_COEFFICIENT = 1;
	/**
	 * distance of propagation of updates
	 */
	public final int PROPAGATE_MAX_DISTANCE=0;
	
	/**
	 * a set of item to take in consideration
	 */
	private Cluster<Advisable<ModelClassType>> iSet;
	/**
	 * strategy for make cluster
	 */
	private ClusterModelMaker<MemoryClassType, ModelClassType> clusterMaker;
	/**
	 * a parameter for adjust exploration
	 */
	private double explorationParameter;
	/**
	 * a parameter for adjust exploration at the time t
	 */
	private double updatedExplorationParameter;
	/**
	 * time of life of the AI
	 */
	private int time=1;

	/**
	 * @param clusterMaker strategy of making cluster
	 * @param items a starting set of items
	 * @param explorationConstantAlpha a exploration constant 
	 */
	public AdvisableConsulter(ClusterModelMaker<MemoryClassType, ModelClassType> clusterMaker,
			Cluster<Advisable<ModelClassType>> items, double explorationConstantAlpha) {
		this.clusterMaker = clusterMaker;
		this.iSet = items;
		explorationParameter = explorationConstantAlpha;
		updatedExplorationParameter = explorationConstantAlpha;
	}

	/**consult an item with CAB(Context-Aware Bandits) Algorithm
	 * @param seekerToNotice a user profile to respect
	 * @return an advisable estimated as best option for know the user
	 */
	public Advisable<ModelClassType> consult(MoldableModel<MemoryClassType, ModelClassType> seekerToNotice) {
		
		Advisable<ModelClassType> best = null;
		double valueTop = 0;
		double evaluatedValue;
		MoldableModel<MemoryClassType, ModelClassType> neightborhoodModel;
		ClusterMoldableModel<MemoryClassType, ModelClassType> clusterForThisAd;

		for (Advisable<ModelClassType> itemToAnalyze : iSet) {// k=1...ct
			clusterForThisAd = clusterMaker.getSubCluster(seekerToNotice, itemToAnalyze, updatedExplorationParameter);// Nit,t(xt,k)
			neightborhoodModel = clusterForThisAd.getClusterModel();
			evaluatedValue = evaluateCluser(neightborhoodModel,seekerToNotice, itemToAnalyze);
//			System.out.println(evaluatedValue);
			if (evaluatedValue > valueTop || best == null) {
				valueTop = evaluatedValue;
				best = itemToAnalyze;
			}
		}
		return best;
	}

	/**evaluate cluster following the CabAlgorithm
	 * @param neightborhoodModel
	 * @param seekerToNotice 
	 * @param itemToAnalyze
	 * @return
	 */
	private double evaluateCluser(MoldableModel<MemoryClassType, ModelClassType> neightborhoodModel,
			MoldableModel<MemoryClassType, ModelClassType> seekerToNotice, Advisable<ModelClassType> itemToAnalyze) {
		double cb = updatedExplorationParameter * neightborhoodModel.getConfidenceBound(itemToAnalyze);
		double similarityCoefficient = neightborhoodModel.getModel().getFuzzyCoefficient(itemToAnalyze.features());
		double userSim= updatedExplorationParameter * seekerToNotice.getConfidenceBound(itemToAnalyze);
		double userCB=seekerToNotice.getModel().getFuzzyCoefficient(itemToAnalyze.features());
		
		double CLUSTER_AFFIDABILTY_COEFFICIENT_Calculated;
		if (seekerToNotice.getLearningDegree()!=0) {
			CLUSTER_AFFIDABILTY_COEFFICIENT_Calculated = neightborhoodModel.getLearningDegree()/seekerToNotice.getLearningDegree();
		}else {
			CLUSTER_AFFIDABILTY_COEFFICIENT_Calculated=1;
		}
		return CLUSTER_AFFIDABILTY_COEFFICIENT*((similarityCoefficient + cb)/neightborhoodModel.getLearningDegree())+
				(1-CLUSTER_AFFIDABILTY_COEFFICIENT)*((userSim+userCB)/seekerToNotice.getLearningDegree());
	}

	/**
	 * @return the exploration parameter
	 */
	public double getExplotationParameter() {
		return explorationParameter;
	}

	/**update the profile with ad and payoff payoff and {@link ClusterMaker} with propagation with constants previously set
	 * @param profile profile to call update method
	 * @param ad item with update the profile
	 * @param payoff payoff estimated with consultation 
	 */
	public void updateWeight(MoldableModel<MemoryClassType,ModelClassType> profile, Advisable<ModelClassType> ad, float payoff){
		updateExplotationParameter();
		profile.update(payoff, ad);
		clusterMaker.update(profile,ad); //the main user is every times updated
		if (profile instanceof MoldableModelNode) {
			Collection<MoldableModelNode<MemoryClassType, ModelClassType>> propagateUpdate = propagateUpdate(payoff, ad,
					(MoldableModelNode<MemoryClassType, ModelClassType>) profile);
			for (MoldableModelNode<MemoryClassType, ModelClassType> moldableModelNode : propagateUpdate) {
				clusterMaker.update(moldableModelNode, ad);// the main user is every times contained
			}
		}
		
	}

	/**
	 * @return explorationParameter updated with time of the system's life 
	 */
	private double updateExplotationParameter() {
		updatedExplorationParameter = explorationParameter * Math.sqrt(Math.log(++time));
		return updatedExplorationParameter;
	}

	/**
	 * @return exploration Parameter updated with time of the system's life 
	 */
	public double getUpdatedExplorationParameter() {
		return updatedExplorationParameter;
	}

	/**
	 * @return get the set of advisable with make consultation
	 */
	public Cluster<Advisable<ModelClassType>> getAdvisableSet() {
		return iSet;
	}

	/**set the set of advisable with make consultation
	 * @param iSet the set of advisable with make consultation
	 */
	public void setAdvisableSet(Cluster<Advisable<ModelClassType>> iSet) {
		this.iSet = iSet;
	}

	/**propagate update of the selected user with breadth-first search
	 * 
	 * @param features item with update the profile
	 * @param user payoff estimated with consultation 
	 * @param payoff starting payoff for propagation 
	 * @return the visited nodes
	 */
	public Collection<MoldableModelNode<MemoryClassType, ModelClassType>> propagateUpdate(float payoff,
			Advisable<ModelClassType> features, MoldableModelNode<MemoryClassType, ModelClassType> user) {
		HashSet<AbstractNode> visited = new HashSet<AbstractNode>();
		HashSet<MoldableModelNode<MemoryClassType, ModelClassType>> updated = new HashSet<MoldableModelNode<MemoryClassType, ModelClassType>>();

		LinkedList<DirectionalLink> toVisit = new LinkedList<DirectionalLink>();
		Collection<DirectionalLink> adiacency = user.getAdiacencyList();
		toVisit.addAll(adiacency);
		visited.add(user);
		int distance = 0;
		int linkLeft = adiacency.size();
		DirectionalLink currentLink;
		MoldableModelNode<MemoryClassType, ModelClassType> currentModelNode;
		double weight;
		AbstractNode target;
		while (!toVisit.isEmpty()) {
			currentLink = toVisit.pop();
			target = currentLink.getTarget();
			if (target instanceof MoldableModelNode) {
				currentModelNode = (MoldableModelNode<MemoryClassType, ModelClassType>) target;
				weight = currentLink.getWeight();
				currentModelNode.update(getPropagationPayoff(payoff, weight,distance), features);
				updated.add(currentModelNode);
			}
			linkLeft--;
			if (distance == PROPAGATE_MAX_DISTANCE) {
				return updated;
			}
			adiacency = target.getAdiacencyList();
			for (DirectionalLink directionalLink : adiacency) {
				target = directionalLink.getTarget();
				if (!visited.contains(target)) {
					visited.add(target);
					toVisit.add(directionalLink);
				}
			}
			if (linkLeft == 0) {
				distance++;
				linkLeft = toVisit.size();
			}
		}
		return updated;
	}

	/**
	 * @param payoff payoff of update users
	 * @param weight weight of the linked edge
	 * @param distance distance form main updated user
	 * @return the calculate payoff on distance "distance"
	 */
	public float getPropagationPayoff(float payoff, double weight, int distance) {
		return (float) (weight * payoff*Math.pow(PROPAGATE_DISTANCE_PAYOFF_CONSERVATION, distance));
	}

}
