package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *
 */
public class Seeker extends MoldableModelNode<LinearMatrix, Vector> implements Cloneable{
	/**
	 * for deserialization
	 */
	private static final long serialVersionUID = 8341604391782511468L;
	/**
	 * 
	 */
	private LinearMatrix memory;
	/**
	 * 
	 */
	private LinearMatrix inverseOfMemory;
	/**
	 * 
	 */
	private Vector payoffVector;
	/**
	 * 
	 */
	private Vector model;
	/**
	 * 
	 */
	private LinearMatrix latestMemoryAdds;
	private int learningDegree=0;
	private double cumulatedPayoff=0;

	/**
	 * @param featuresSize size of the matrix NxN
	 * @param id an identificator for same items
	 */
	public Seeker(int featuresSize, String id) {
		super(id);
		this.memory = new LinearMatrix(featuresSize);
		this.payoffVector = new Vector(featuresSize);
		model = new Vector(featuresSize);
		inverseOfMemory = new LinearMatrix(featuresSize);
	}
	
	/**
	 * @param id an identificator for same items
	 */
	private Seeker(String id) {
		super(id);
	}

	/**
	 * @param featuresSize  size of the matrix NxN
	 */
	public Seeker(int featuresSize) {
		super(null);
		this.memory = new LinearMatrix(featuresSize);
		this.payoffVector = new Vector(featuresSize);
		model = new Vector(featuresSize);
		inverseOfMemory = new LinearMatrix(featuresSize);
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#memory()
	 */
	@Override
	public LinearMatrix memory() {//
		return memory;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getModel()
	 */
	@Override
	public Vector getModel() {
		if (model == null) {
			updateModel();
		}
		return model;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getPayoffModel()
	 */
	@Override
	public Vector getPayoffModel() {
		try {
			return payoffVector.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getInverseOfMemory()
	 */
	@Override
	public LinearMatrix getInverseOfMemory() {
		if (latestMemoryAdds!=null) {
			inverseOfMemory.addToAnInverse(latestMemoryAdds);
			latestMemoryAdds=null;
		}
		return inverseOfMemory;
	}

	/**add an already learned memory,but not learned by this
	 * @param memoryToAdd memory to add
	 * @param payoffVector vector that represent like in CAB all the offered items*payoff
	 */
	private void addMemory(LinearMatrix memoryToAdd, Vector payoffVector) {
		LinearMatrix cleanMemory = memoryToAdd.getSubIdentity();
		memory.sum(cleanMemory);
		addToInverseMemory(cleanMemory);
		this.payoffVector.sum(payoffVector);
	}

	/**
	 * @param cleanMemory memory already cleared by the identity
	 */
	private void addToInverseMemory(LinearMatrix cleanMemory) {
		if (latestMemoryAdds==null) {
			latestMemoryAdds=new LinearMatrix(memory.columnNumber);
		}
		latestMemoryAdds.sum(cleanMemory);
		
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#update(float, it.insubria.cabAlgorithm.dataStructures.Advisable)
	 */
	@Override
	public void update(float payoff, Advisable<Vector> objectOffered) {
		Vector features = objectOffered.features();
		updateDataStructures(payoff, features);
		
	}

	/**learn a features
	 * @param payoff feedback of learning
	 * @param features what to learn
	 */
	private void updateDataStructures(float payoff, Vector features) {
		addMemory(features);
		boolean considerablePayoff = Math.abs(payoff) > 0.000001;
		if (considerablePayoff) {
			addPayoff(payoff, features);
		}
		updateModel();
		
	}

	/**learn like in CAB
	 * @param payoff payoff feedback of learning
	 * @param features what to learn
	 */
	private void addPayoff(float payoff, Vector features) {
		if (features.size() != payoffVector.size()) {
			throw new IllegalArgumentException("features must have same length of payoffVector");
		}
		LinearMatrix mult = features.getMult(payoff);
		payoffVector.sum(mult);
		cumulatedPayoff+=payoff;
		
	}

	/**learn like in CAB
	 * @param features what to learn
	 */
	private void addMemory(Vector features) {
		LinearMatrix advisableMatrix = features.toSquareMatrix();
		memory.sum(advisableMatrix);
		inverseOfMemory.shermanMorrison(features);
		learningDegree++;
	}

	/**
	 * Construct the model learn like in CAB
	 */
	private void updateModel() {
		LinearMatrix multRes = inverseOfMemory.getMult(payoffVector);
		if (model == null) {
			model = new Vector(multRes.content);
		} else {
			model.setContent(multRes.content);
		}
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getConfidenceBound(it.insubria.cabAlgorithm.dataStructures.Advisable)
	 */
	@Override
	public double getConfidenceBound(Advisable<Vector> anItem) {
		Vector features = anItem.features();
		return Math.sqrt(Vector.dotProduct(features.content, inverseOfMemory.getMult(features).content));
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#addMemory(it.insubria.cabAlgorithm.dataStructures.MoldableModel)
	 */
	@Override
	public void addMemory(MoldableModel<LinearMatrix, Vector> sameMemoryElement) {
		if (sameMemoryElement instanceof Seeker) {
			this.addMemory(sameMemoryElement.memory(), ((Seeker) sameMemoryElement).payoffVector);
		}else {
			this.addMemory(sameMemoryElement.memory(), sameMemoryElement.getPayoffModel());
		}
		learningDegree+=sameMemoryElement.getLearningDegree();
		cumulatedPayoff+=sameMemoryElement.getCumulatePayoff();
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#subtractMemory(it.insubria.cabAlgorithm.dataStructures.MoldableModel)
	 */
	@Override
	public void subtractMemory(MoldableModel<LinearMatrix, Vector> sameMemoryElement) {
		if (sameMemoryElement instanceof Seeker) {
			this.subtractMemory(sameMemoryElement.memory(), ((Seeker) sameMemoryElement).payoffVector);
		}else {
			this.subtractMemory(sameMemoryElement.memory(), sameMemoryElement.getPayoffModel());
		}
		learningDegree-=sameMemoryElement.getLearningDegree();
		cumulatedPayoff-=sameMemoryElement.getCumulatePayoff();
	}

	/**
	 * 
	 * sub an already learned memory,but not learned by this
	 * @param memoryToSub memory to add
	 * @param payoffModel vector that represent like in CAB all the offered items*payoff
	 *
	 */
	private void subtractMemory(LinearMatrix memoryToSub, Vector payoffModel) {
		LinearMatrix cleanMemory = memoryToSub.getSub(new LinearMatrix(memoryToSub.getColumnNumber()));
		memory.sub(cleanMemory);
		inverseOfMemory.addToAnInverse(cleanMemory);
		this.payoffVector.sub(payoffModel);
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getFeaturesSize()
	 */
	@Override
	public int getFeaturesSize() {
		return payoffVector.size();
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModelNode#clone()
	 */
	@Override
	public MoldableModel<LinearMatrix, Vector> clone() throws CloneNotSupportedException {
		Seeker clone = new Seeker(this.getId());
		clone.memory=memory.clone();
		clone.inverseOfMemory=inverseOfMemory.clone();
		clone.payoffVector=payoffVector.clone();
		clone.model=null;
		clone.cumulatedPayoff=cumulatedPayoff;
		clone.learningDegree=learningDegree;
//		clone.adiacencyList=(HashSet<DirectionalLink>) adiacencyList.clone();// i don't need it(too expensive)
		return clone;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getLearningDegree()
	 */
	@Override
	public int getLearningDegree() {
		return learningDegree;
	}

	
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getCumulatePayoff()
	 */
	@Override
	public double getCumulatePayoff() {
		return cumulatedPayoff;
	}

}
