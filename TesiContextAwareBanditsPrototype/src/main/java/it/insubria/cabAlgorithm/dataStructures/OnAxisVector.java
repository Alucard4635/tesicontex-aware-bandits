package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 * A fake vector , it represent a vector with all zeros exept one with indicated intensity, 1 otherwise 
 */
public class OnAxisVector implements FuzzyEquals {
	/**
	 * index of the non-zero element starting with 0
	 */
	private int axisIndex;
	/**
	 * intensity of the non-zero element's index  starting with 0
	 */
	private float intensity;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return (Integer.toString(axisIndex)+intensity).hashCode();
	}
	
	/**initialize an {@link OnAxisVector} with intensity=1
	 * @param axisIndex index of the non-zero element starting with 0
	 */
	public OnAxisVector(int axisIndex) {
		this.axisIndex = axisIndex;
		intensity=1f;
	}
	/**
	 * @param axisIndex index of the non-zero element starting with 0
	 * @param intensity
	 */
	public OnAxisVector(int axisIndex,float intensity) {
		this.axisIndex = axisIndex;
		this.intensity = intensity;
	}

	/**
	 * @return index of the non-zero element starting with 0
	 */
	public int getAxisIndex() {
		return axisIndex;
	}

	/**
	 * @param axisIndex index of the non-zero element starting with 0
	 */
	public void setAxisIndex(int axisIndex) {
		this.axisIndex = axisIndex;
	}
	
	/**convert the fake vector to a real one
	 * @param howLongIsTheVector  length of vector space 
	 * @return converted vector
	 */
	public Vector toVector(int howLongIsTheVector){
		float[] linearContent=new float[howLongIsTheVector];
		linearContent[axisIndex]=intensity;
		return new Vector(linearContent);
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.FuzzyEquals#getFuzzyCoefficient(java.lang.Object)
	 */
	@Override
	public double getFuzzyCoefficient(Object toCompare){
		if (toCompare instanceof Vector) {
			Vector v=(Vector) toCompare;
			float[] content = v.content;
			if (content.length<=axisIndex) {
				return 0;
			}
			return content[axisIndex]*intensity;
		}
		if (toCompare instanceof OnAxisVector) {
			OnAxisVector v=(OnAxisVector) toCompare;
			if (axisIndex!=v.axisIndex) {
				return 0;
			}
			return intensity*v.intensity;
		}
		return 0;
	}

	/**
	 * @return intensity of the non-zero element's index  starting with 0
	 */
	public float getIntensity() {
		return intensity;
	}

	/**
	 * @param intensity intensity of the non-zero element's index  starting with 0
	 */
	public void setIntensity(float intensity) {
		this.intensity = intensity;
	}

	/**
	 * @param multiplicator number
	 * @return a new vector with  {@code this.intensity=this.intensity*multiplicator}
	 */
	public OnAxisVector getMult(float multiplicator) {
		return new OnAxisVector(axisIndex,intensity*multiplicator);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof OnAxisVector) {
			OnAxisVector onAxisVector = (OnAxisVector)obj;
			return (axisIndex==onAxisVector.axisIndex)&&(intensity-onAxisVector.intensity<0.000001);
		}
		return super.equals(obj);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return Integer.toString(axisIndex)+" "+intensity;
	}
}
