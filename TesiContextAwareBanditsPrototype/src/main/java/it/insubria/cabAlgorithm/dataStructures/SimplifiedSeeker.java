package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *  a particular model which use simplified matrix(only diagonal), and simplified vector {@link OnAxisVector} 
 */
public class SimplifiedSeeker extends MoldableModelNode<DiagonalMatrix,OnAxisVector> {
	/**
	 * for deserialization
	 */
	private static final long serialVersionUID = -7869491384060346995L;
	/**
	 * a diagonal learning system
	 */
	private DiagonalMatrix memory;
	/**
	 * a cumulated payoff vector
	 */
	private Vector payoffVector;
	/**
	 * a cached model produced like in CAB
	 */
	private Vector model;
	private int learningDegree=0;
	private double cumulatedPayoff=0;

	/**create a simplified model
	 * @param featuresSize size of diagonal and advisable vectors
	 * @param id an identificator
	 */
	public SimplifiedSeeker(int featuresSize,String id) {
		super(id);
		memory=new DiagonalMatrix(featuresSize);
		payoffVector=new Vector(featuresSize);
		model=new Vector(featuresSize);
	}
	/**used for clone
	 * @param id an identificator
	 */
	private SimplifiedSeeker(String id) {
		super(id);
	}
	
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getFeaturesSize()
	 */
	public int getFeaturesSize(){
		return payoffVector.size();
	}
	
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getModel()
	 */
	@Override
	public Vector getModel() {
		return model;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getPayoffModel()
	 */
	@Override
	public Vector getPayoffModel() {
		try {
			return payoffVector.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#memory()
	 */
	@Override
	public DiagonalMatrix memory() {
		return memory;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getInverseOfMemory()
	 */
	@Override
	public DiagonalMatrix getInverseOfMemory() {
		return memory.getInverse();
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#addMemory(it.insubria.cabAlgorithm.dataStructures.MoldableModel)
	 */
	@Override
	public void addMemory(MoldableModel<DiagonalMatrix,OnAxisVector> modelWhereExtractMemory) {
		memory.sum(modelWhereExtractMemory.memory());
		memory.subIdentity();
		learningDegree+=modelWhereExtractMemory.getLearningDegree();
		cumulatedPayoff+=modelWhereExtractMemory.getCumulatePayoff();
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#update(float, it.insubria.cabAlgorithm.dataStructures.Advisable)
	 */
	@Override
	public void update(float payoff, Advisable<OnAxisVector> objectOffered) {
		OnAxisVector features = objectOffered.features();
		updateDataStructures(payoff, features);
		learningDegree++;
		cumulatedPayoff+=payoff;
	}

	/**update data structure with good performance
	 * @param payoff feedback for learning
	 * @param features from which come payoff 
	 */
	private void updateDataStructures(float payoff, OnAxisVector features) {
		addMemory(features);
		if (payoff>0.000001) {
			addPayoff(payoff, features);
		}
		updateModel();
		
	}

	/**
	 * create an updated model like in CAB algorithm
	 */
	private void updateModel() {
		model=memory.getInverse().getMult(payoffVector);
	}
	
	/**learn some features
	 * @param features to learn
	 */
	private void addMemory(OnAxisVector features) {
		memory.diagonal.sum(features);
		
	}
	
	/**
	 * @param payoff feedback for learning
	 * @param features from which come payoff 
	 */
	private void addPayoff(float payoff, OnAxisVector features) {
		payoffVector.sum(features.getMult(payoff));
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getConfidenceBound(it.insubria.cabAlgorithm.dataStructures.Advisable)
	 */
	@Override
	public double getConfidenceBound(Advisable<OnAxisVector> anItem) {
		OnAxisVector features = anItem.features();
		return  Math.sqrt(features.getIntensity()*getInverseOfMemory().getMult( features));

	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#subtractMemory(it.insubria.cabAlgorithm.dataStructures.MoldableModel)
	 */
	@Override
	public void subtractMemory(
			MoldableModel<DiagonalMatrix, OnAxisVector> memory) {
		this.memory.sub(memory.memory());
		this.memory.sumIdentity();
		learningDegree-=memory.getLearningDegree();
		cumulatedPayoff-=memory.getCumulatePayoff();
		
	}


	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#toString()
	 */
	@Override
	public String toString() {
		return getId();
	}


	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModelNode#clone()
	 */
	@Override
	public MoldableModel<DiagonalMatrix, OnAxisVector> clone() throws CloneNotSupportedException {
		SimplifiedSeeker clone = new SimplifiedSeeker(getId());
		clone.memory=this.memory.clone();
		clone.model=model.clone();
		clone.payoffVector=payoffVector.clone();
		clone.cumulatedPayoff=cumulatedPayoff;
		clone.learningDegree=learningDegree;
//		clone.adiacencyList=(HashSet<DirectionalLink>) adiacencyList.clone();// i don't need it(too expensive)
		return clone;
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getLearningDegree()
	 */
	@Override
	public int getLearningDegree() {
		return learningDegree;
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.MoldableModel#getCumulatePayoff()
	 */
	@Override
	public double getCumulatePayoff() {
		return cumulatedPayoff;
	}
}
