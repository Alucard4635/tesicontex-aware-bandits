package it.insubria.cabAlgorithm.dataStructures;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import it.insubria.socialNetwork.AbstractNode;
import it.insubria.socialNetwork.DirectionalLink;

/**
 * @author Andrea Marisio<br>
 *
 * @param <MemoryClassType> type of Memory crucial for compatibility issues
 * @param <AdvisableClassType> type of acceptable features for learning
 */
public abstract class MoldableModelNode<MemoryClassType, AdvisableClassType> extends AbstractNode implements MoldableModel<MemoryClassType, AdvisableClassType> {
	/**
	 * For correct Deserialization
	 */
	private static final long serialVersionUID = 4322479677860400916L;
	
	/**
	 * type of data structure for handle relations
	 */
	protected HashSet<DirectionalLink> adiacencyList=new HashSet<DirectionalLink>();
		

	/**
	 * @param id identificator, same id means same item
	 */
	public MoldableModelNode(String id) {
		super(id);
	}
	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#findAdiacentNode(java.lang.String)
	 */
	@Override
	public AbstractNode findAdiacentNode(String targetID) {
		for (DirectionalLink directionalLink : adiacencyList) {
			AbstractNode target = directionalLink.getTarget();
			if (target.getId().equals(targetID)) {
				return target;
			}
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#getAdiacencyList()
	 */
	@Override
	public Collection<DirectionalLink> getAdiacencyList() {
		return new HashSet<DirectionalLink>(adiacencyList);
	}

	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#setAdiacencyList(java.util.Collection)
	 */
	@Override
	public void setAdiacencyList(Collection<DirectionalLink> list) {
		adiacencyList=new HashSet<DirectionalLink>(list);
	}
	
	/* (non-Javadoc)
	 * @see it.insubria.socialNetwork.AbstractNode#addDirectionalLink(it.insubria.socialNetwork.DirectionalLink)
	 */
	@Override
	public void addDirectionalLink(DirectionalLink link) {
		adiacencyList.add(link);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public abstract MoldableModel<MemoryClassType, AdvisableClassType> clone() throws CloneNotSupportedException ;
	
	
}
