package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *	this class is made for an efficient system for use diagonal matrix, it don't extends Vector for conserve his integrity
 */
public class DiagonalMatrix implements Cloneable{
	/**
	 * the diagonal
	 */
	protected Vector diagonal;

	/**
	 * @param rowOrColumnNumber number of element in the diagonal
	 */
	public DiagonalMatrix(int rowOrColumnNumber) {
		float f = 1f;
		float[] diagonalContent = MatrixToolkit.getInizializedArray(rowOrColumnNumber, f);
		diagonal = new Vector(diagonalContent);
	}

	/**
	 * @param content in the diagonal
	 */
	public DiagonalMatrix(float[] content) {
		diagonal = new Vector(content);
	}

	/**
	 * @return get a clone of diagonal, null if not clonable {@link CloneNotSupportedException}
	 */
	public Vector getDiagonal() {
		try {
			return diagonal.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param diagonal effective diagonal vector to set
	 */
	public void setDiagonal(Vector diagonal) {
		this.diagonal = diagonal;
	}

	/**
	 * @param diag DiagonalMatrix to sum
	 */
	public void sum(DiagonalMatrix diag) {
		diagonal.sum(diag.diagonal);
	}

	/**
	 * @param diag DiagonalMatrix to sum
	 * @return a new instance that is the sum of DiagonalMatrixs
	 */
	public DiagonalMatrix getSum(DiagonalMatrix diag) {
		return new DiagonalMatrix(diagonal.getSum(diag.diagonal).content);

	}

	/**
	 * @param diag DiagonalMatrix to sub
	 */
	public void sub(DiagonalMatrix diag) {
		diagonal.sub(diag.diagonal);

	}

	/**
	 * @param diag DiagonalMatrix to sub
	 * @return a new instance that is the sun of DiagonalMatrixs
	 */
	public DiagonalMatrix getSub(DiagonalMatrix diag) {
		return new DiagonalMatrix(diagonal.getSub(diag.diagonal).content);
	}

	/**
	 * @param diag DiagonalMatrix to multiply
	 */
	public void mult(DiagonalMatrix diag) {
		float[] contentA = diagonal.content;
		float[] contentB = diag.diagonal.content;
		diagonal.setContent(MatrixToolkit.getMultArrayByComponent(contentA, contentB));
	}
	
	


	/**
	 * @param diag DiagonalMatrix to multiply
	 * @return a new instance that is the result of DiagonalMatrixs multiplication 
	 */
	public DiagonalMatrix getMult(DiagonalMatrix diag) {
		float[] contentA = diagonal.content;
		float[] contentB = diag.diagonal.content;
		float[] result = MatrixToolkit.getMultArrayByComponent(contentA, contentB);
		return new DiagonalMatrix(result);
	}

	/**
	 * @param diag DiagonalMatrix to divide
	 */
	public void div(DiagonalMatrix diag) {
		float[] contentA = diagonal.content;
		float[] contentB = diag.diagonal.content;
		diagonal.setContent(MatrixToolkit.getDivArrayByComponent(contentA, contentB));
	}

	/**
	 * @param diag DiagonalMatrix to divide
	 * @return a new instance that is the result of DiagonalMatrixs division 
	 */
	public DiagonalMatrix getDiv(DiagonalMatrix diag) {
		float[] contentA = diagonal.content;
		float[] contentB = diag.diagonal.content;
		float[] result = MatrixToolkit.getDivArrayByComponent(contentA, contentB);
		return new DiagonalMatrix(result);
	}

	/**
	 * @param real coefficient of multiplication
	 */
	public void mult(float real) {
		diagonal.mult(real);
	}

	/**
	 * @param real coefficient of multiplication
	 * @return a new instance that is the result of DiagonalMatrixs multiplication 
	 */
	public DiagonalMatrix getMult(float real) {
		return new DiagonalMatrix(diagonal.getMult(real).content);
	}

	/**
	 * @param real
	 */
	public void div(float real) {
		diagonal.div(real);
	}

	/**
	 * @param real coefficient of division
	 * @return a new instance that is the result of DiagonalMatrixs division 
	 */
	public DiagonalMatrix getDiv(float real) {
		return new DiagonalMatrix(diagonal.getDiv(real).content);

	}

	/**invert the diagonal, reversing all member on diagonal
	 * @return inverse matrix
	 */
	public DiagonalMatrix getInverse() {
		float[] diagonalContent = MatrixToolkit.getArrayReciprocal(diagonal.content);
		return new DiagonalMatrix(diagonalContent);
	}

	/**
	 * @param vector a Vector for a matrix to vector multiplication
	 * @return a new instance that is the result of DiagonalMatrix with a Vector multiplication 
	 */
	public Vector getMult(Vector vector) {
		float[] contentA = diagonal.content;
		float[] contentB = vector.content;
		float[] result = MatrixToolkit.getMultArrayByComponent(contentA, contentB);
		return new Vector(result);
	}

	/**
	 * @param features
	 * @return a new instance that is the result of DiagonalMatrix with a Simplified Vector (all zero component exept one) multiplication 
	 */
	public float getMult(OnAxisVector features) {
		return diagonal.content[features.getAxisIndex()] * features.getIntensity();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder b=new StringBuilder();
		for (int i = 0; i < diagonal.content.length; i++) {
			for (int j = 0; j < diagonal.content.length; j++) {
				if (i==j) {
					b.append(diagonal.content[i]);
				}else {
					b.append(0);
				}
			}
			b.append('\n');
		}
		return b.toString();
	}

	/**
	 * Subtract 1 on diagonal
	 */
	public void subIdentity() {
		MatrixToolkit.subArray(1, diagonal.content);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public DiagonalMatrix clone() throws CloneNotSupportedException {
		return new DiagonalMatrix(this.diagonal.content.clone());
	}

	/**
	 * add 1 on diagonal
	 */
	public void sumIdentity() {
		MatrixToolkit.sumArray(1, diagonal.content);
	}
}
