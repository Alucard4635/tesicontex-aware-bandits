package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *A simulated advisable vector with all zero and a non zeros elements
 */
public class OnAxisAdvisable implements Advisable<OnAxisVector> {

	/**
	 * A simulated vector with all zero and a non zeros elements, used for describe items
	 */
	private OnAxisVector features;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return features.hashCode();
	}
	/**
	 * @param axis axis index starting by zero of the fake vector 
	 */
	public OnAxisAdvisable(int axis) {
		features = new OnAxisVector(axis);
		
	}
	
	/**
	 * @param axis axis index starting by zero of the fake vector
	 * @param intensity value of the non-zero features
	 */
	public OnAxisAdvisable(int axis, float intensity) {
		features = new OnAxisVector(axis, intensity);
		
	}
	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.Advisable#features()
	 */
	@Override
	public OnAxisVector features() {
		return features;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.Advisable#setFeatures(java.lang.Object)
	 */
	@Override
	public void setFeatures(OnAxisVector features) {
		this.features = features;
		
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return features.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof OnAxisAdvisable) {
			OnAxisAdvisable advisable = (OnAxisAdvisable)obj;
			return features.equals(advisable.features);
		}
		
		return super.equals(obj);
	}
}
