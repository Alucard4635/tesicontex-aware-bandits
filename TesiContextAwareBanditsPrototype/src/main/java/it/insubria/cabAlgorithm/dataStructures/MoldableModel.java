package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *
 * @param <MemoryClassType> type of Memory crucial for compatibility issues
 * @param <AdvisableClassType> type of acceptable features for learning
 */
public interface MoldableModel<MemoryClassType,AdvisableClassType> extends Cloneable{
	/**sum an already learned memory
	 * @param memory memory to add
	 */
	public void addMemory(MoldableModel<MemoryClassType,AdvisableClassType> memory);
	/**sub an already learned memory
	 * @param memory memory to sub
	 */
	public void subtractMemory(MoldableModel<MemoryClassType,AdvisableClassType> memory);
	/**lean an offer with payoff a
	 * @param payoff respond of object of study feedback
	 * @param objectOffered object to remember
	 */
	public void update(float payoff, Advisable<AdvisableClassType> objectOffered);
	/**return confidence bound of the model for object anItem
	 * @param anItem object which get confidence bound
	 * @return the result of the estimated  confidence bound
	 */
	public double getConfidenceBound(Advisable<AdvisableClassType> anItem) ;
	/**
	 * @return a vector representing the object of study
	 */
	public Vector getModel() ;
	/**
	 * @return a vector representing payoffs and items offered 
	 */
	public Vector getPayoffModel() ;
	/**
	 * @return the reference of structure of learning
	 */
	public MemoryClassType memory() ;
	/**
	 * @return the inverse of structure of learning
	 */
	public MemoryClassType getInverseOfMemory() ;
	/**
	 * @return a deep passage of value
	 * @throws CloneNotSupportedException
	 */
	public MoldableModel<MemoryClassType, AdvisableClassType> clone() throws CloneNotSupportedException;
	/**
	 * @return size of features that can learn
	 */
	public int getFeaturesSize();
	/**a light mode to identify equals object
	 * @param obj an object to compare
	 * @return true if this is equals to obj
	 */
	@Override
	public boolean equals(Object obj);
	/**
	 * @return an identificative int 
	 */
	@Override
	public int hashCode();
	
	/**
	 * @return number of learning steps
	 */
	public int getLearningDegree();
	
	/**
	 * @return number of payoff learned
	 */
	public double getCumulatePayoff();
}
