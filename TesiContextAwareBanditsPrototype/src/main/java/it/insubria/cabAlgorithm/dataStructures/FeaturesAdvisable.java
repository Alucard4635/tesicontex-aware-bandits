package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *An {@link Advisable} with feature like a vector
 */
public class FeaturesAdvisable implements Advisable<Vector> {
	
	/**
	 * an identificator for hashcode and equals
	 */
	private String id;
	/**
	 * content of advisable
	 */
	private Vector features;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * @param size size of the features
	 * @param id an identificator for hashcode and equals
	 */
	public FeaturesAdvisable(int size,String id) {
		this.id = id;
		features = new Vector(size);
	}

	/**
	 * @param features content of the features
	 * @param id an identificator for hashcode and equals
	 */
	public FeaturesAdvisable(float[] features,String id) {
		this.id = id;
		this.features = new Vector(features);
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.Advisable#features()
	 */
	@Override
	public Vector features() {
		return features;
	}

	/* (non-Javadoc)
	 * @see it.insubria.cabAlgorithm.dataStructures.Advisable#setFeatures(java.lang.Object)
	 */
	@Override
	public void setFeatures(Vector features) {
		try {
			this.features = features.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FeaturesAdvisable) {
			FeaturesAdvisable f=(FeaturesAdvisable) obj;

			return id.equals(f.id);
		}
		return super.equals(obj);
	}
}
