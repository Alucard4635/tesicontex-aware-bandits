package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio
 *an interface for indicate that class can made an fuzzy equals evaluation
 */
public interface FuzzyEquals {
	/**indicate the similarity of two items,0 for different objects, &gt;0 otherwise, for example for two vector this method can implements a dot product, or simply the cosine
	 * @param toCompare item to compare
	 * @return a double that indicate the similarity of two items,0 for different objects, &gt;0 otherwise
	 */
	public double getFuzzyCoefficient(Object toCompare);
}
