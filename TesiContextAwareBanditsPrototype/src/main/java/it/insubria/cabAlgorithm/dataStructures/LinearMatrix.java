package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio<br>
 *this is a version of a one dimensional array matrix, this matrix is made like this for a GPU use 
 */
public class LinearMatrix implements Cloneable{
	/**
	 * content of a matrix read by lines from right to left
	 */
	protected float[] content;
	/**
	 * number of column
	 */
	protected int columnNumber;
	
	/**
	 * @param linearContent linear content of matrix
	 * @param columnsNum number of columns
	 */
	public LinearMatrix(float[] linearContent, int columnsNum) {
		content = linearContent;
		columnNumber = columnsNum;
		if (linearContent.length%columnsNum!=0) {
			throw new IllegalArgumentException("content length mismatch with a matrix with "+columnsNum+" columns");
		}
	}

	/**the content is a identity matrix NxN
	 * @param columns number of columns
	 */
	public LinearMatrix(int columns) {
		columnNumber = columns;
		content = 	MatrixToolkit.getIdentity(columns);
	}
	
	/**
	 * @param columns number of columns
	 * @return a LinearMatrix NxN with an identity
	 */
	public static LinearMatrix getIdentity(int columns){
		return new LinearMatrix(columns);
	}

	/**create a LinearMatrix rowXcolumns
	 * @param rows row number of matrix
	 * @param columns columns number of matrix
	 */
	public LinearMatrix(int rows, int columns) {
		columnNumber = columns;
		content = new float[rows*columns];
	}

	
	/**
	 * transpose current matrix
	 */
	public void transpose() {
		float[] data = MatrixToolkit.getTransposeMatrix(content, columnNumber);
		int newColumnNumber = content.length / columnNumber;
		content=data;
		columnNumber=newColumnNumber;
	}
	
	/**sum of matrix
	 * @param B to sum at this matrix
	 */
	public void sum(LinearMatrix B) {
		this.content=MatrixToolkit.getSumArrayByComponent(content, B.content);
	}
	/**sub of matrix
	 * @param B to sub at this matrix
	 */
	public void sub(LinearMatrix B) {
		LinearMatrix sub =this.getSub( B);
		this.content=sub.content;
		sub.content=null;
	}
	/**multiply by a real this matrix
	 * @param real multiplier
	 */
	public void mult(float real){
		this.content=MatrixToolkit.getMultArray(real,content);
	}
	/**divide by a real this matrix
	 * @param real divisor 
	 */
	public void div(float real){
		this.content=MatrixToolkit.getDivArray(real,content);
	}
	
	/**
	 * @param real multiplier
	 * @return a new instance matrix result of multiply by a real this matrix
	 */
	public LinearMatrix getMult(float real){
		return new LinearMatrix( MatrixToolkit.getMultArray(real,content),this.getColumnNumber());
	}
	
	
	/**
	 * @param real divisor
	 * @return a new instance matrix result of Division by a real this matrix
	 */
	public LinearMatrix getDiv(float real){
		return new LinearMatrix(MatrixToolkit.getDivArray(real,content),this.getColumnNumber());
	}
	


	/**
	 * @param B matrix to add
	 * @return a new instance matrix result of adding this matrix with another
	 */
	public LinearMatrix getSum( LinearMatrix B){
		
		float[] contentA = content;
		float[] contentB = B.content;
		int columnNumberA = this.columnNumber;
		int columnNumberB = B.columnNumber;
		int lengthA = contentA.length;
		if (lengthA != contentB.length && columnNumberA == columnNumberB) {
			throw new IllegalArgumentException("size of first argument mismatch with second argument's one");
		}
		return new LinearMatrix(MatrixToolkit.getSumArrayByComponent(contentA, contentB),columnNumberA);
	}

	
	
	/**
	 * @param B matrix to sub
	 * @return new instance matrix result of subtract this matrix with another
	 */
	public LinearMatrix getSub( final LinearMatrix B){
		float[] contentA = content;
		float[] contentB = B.content;
		int columnNumberA = this.getColumnNumber();
		return new LinearMatrix(MatrixToolkit.getSubArrayByComponent(contentA, contentB),columnNumberA);
	}



	/**
	 * @return a new instance of a transpose matrix
	 */
	public LinearMatrix getTranspose() {
		float[] data = MatrixToolkit.getTransposeMatrix(content, columnNumber);
		int newColumnNumber = content.length / columnNumber;
		return new LinearMatrix(data, newColumnNumber);

	}

	/**A multiplication of matrix right to left, this*B, with this and B are matrix
	 * @param B multiplier
	 * @return a new instance matrix result of multiply this matrix with another
	 */
	public void mult(LinearMatrix B) {
		int columnOfB = B.getColumnNumber();
		float[] result = MatrixToolkit.getMatrixMultiplication(content, B.content,
				this.getColumnNumber(), columnOfB);
		content=result;
		columnNumber=columnOfB;
	}
	

	/**A multiplication of matrix right to left, this*B, with this and B are matrix
	 * @param B multiplier
	 * @return a new instance matrix result of multiply this matrix with another
	 */
	public LinearMatrix getMult(LinearMatrix B) {
		int columnOfB = B.getColumnNumber();
		float[] result = MatrixToolkit.getMatrixMultiplication(content, B.content,
				this.getColumnNumber(), columnOfB);
		return new LinearMatrix(result, columnOfB);
	}


	
	/**
	 * @return a new instance matrix result of inversion
	 */
	public LinearMatrix getInverse(){
		return new LinearMatrix(invertSqareMatrix(content, columnNumber), columnNumber);
	}


	/**
	 * @param matrix content to invert
	 * @param columnNumber number Of column of linear matrix
	 * @return a matrix content result of inversion
	 */
	public static float[] invertSqareMatrix(final float matrix[],
			int columnNumber) {
		int n = columnNumber;
		int dataLength = matrix.length;
		float result[] = new float[dataLength];
		float b[][] = new float[n][n];
		int index[] = new int[columnNumber];
		for (int i = 0; i < n; ++i){
			b[i][i] = 1;
		}
		// Transform the matrix into an upper triangle
		float[] matrixToInvert=new float[dataLength];
		matrixToInvert=matrix.clone();
	
		gaussian(matrixToInvert, columnNumber, index);
	
		// Update the matrix b[i][j] with the ratios stored
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					b[index[j]][k] -= matrixToInvert[index[j] * columnNumber + i]* b[index[i]][k];
				}
			}
		}
		// Perform backward substitutions
		for (int i = 0; i < n; ++i) {
			result[(n - 1) * columnNumber + i] = b[index[n - 1]][i]
					/ matrixToInvert[index[n - 1] * columnNumber + (n - 1)];
			for (int j = n - 2; j >= 0; --j) {
				result[j * columnNumber + i] = b[index[j]][i];
				for (int k = j + 1; k < n; ++k) {
					result[j * columnNumber + i] -= matrixToInvert[index[j]
							* columnNumber + k]
							* result[k * columnNumber + i];
				}
				result[j * columnNumber + i] /= matrixToInvert[index[j]
						* columnNumber + j];
			}
		}
		return result;
	}

	
	// Method to carry out the partial-pivoting Gaussian
	// elimination. Here index[] stores pivoting order.
	/**Method to carry out the partial-pivoting Gaussian
	 * 	elimination. Here index[] stores pivoting order.
	 * @param matrixToResolve content to invert
	 * @param columnNumber number Of column of linear matrix
	 * @param index stores pivoting order.
	 */
	public static void gaussian(float matrixToResolve[], int columnNumber,
			int index[]) {
		int n = index.length;
		float c[] = new float[n];
		// Initialize the index
		for (int i = 0; i < n; ++i) {
			index[i] = i;
		}
		// Find the rescaling factors, one from each row
		for (int i = 0; i < columnNumber; ++i) {
			float c1 = 0;
			for (int j = 0; j < columnNumber; ++j) {
				float c0 = Math.abs(matrixToResolve[i * columnNumber + j]);
				if (c0 > c1) {
					c1 = c0;
				}
			}
			c[i] = c1;
		}
		// Search the pivoting element from each column
		int k = 0;
		for (int j = 0; j < n - 1; ++j) {
			float pi1 = 0;
			for (int i = j; i < n; ++i) {
				float pi0 = Math.abs(matrixToResolve[index[i] * columnNumber
						+ j]);
				pi0 /= c[index[i]];

				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}
			// Interchange rows according to the pivoting order
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;

			for (int i = j + 1; i < n; ++i) {
				float pj = matrixToResolve[index[i] * columnNumber + j]
						/ matrixToResolve[index[j] * columnNumber + j];
				// Record pivoting ratios below the diagonal
				matrixToResolve[index[i] * columnNumber + j] = pj;
				// Modify other elements accordingly
				for (int l = j + 1; l < n; ++l) {
					matrixToResolve[index[i] * columnNumber + l] -= pj* matrixToResolve[index[j] * columnNumber + l];
				}
			}
		}
	}


	/**
	 * @param features to make sharman morrison x*x^T
	 */
	public void shermanMorrison( Vector features) {

//		GPUVector v=new GPUVector(this.getMult( features).getContent());
//		GPULinearMatrix squareMatrix = v.toSquareMatrix();
//		
//		squareMatrix.div(1+GPUVector.dotProduct(features.content, v.content));
//		this.sub(squareMatrix);
		
		LinearMatrix numerator = this.getMult(features.toSquareMatrix().getMult( this));
		float dotProduct = Vector.dotProduct(features.content,this.getMult( features).content);
		numerator.div(1+dotProduct);
		this.sub(numerator);
	}
	/**this Use Inverse of the sum of matrices for make the inverse of a addition of matrix (A+B)^-1
	 * @param toAdd a matrix to add to an inverted matrix
	 */
	public void addToAnInverse(LinearMatrix toAdd){
		LinearMatrix hg = toAdd.getMult(this);
		float g=1/(1+hg.trace());
		this.sub(this.getMult(hg).getMult(g));
	}
	
	/**this Use Inverse of the sum of matrices for make the inverse of a subtraction of matrix
	 * @param toSub a matrix to subtract to an inverted matrix
	 */
	public void subtractToAnInverse(LinearMatrix toSub){
		this.addToAnInverse( toSub.getMult(-1));
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return MatrixToolkit.matrixToString(content, columnNumber);
	}
	
	/**
	 * @return value of columns number
	 */
	public int getColumnNumber() {
		return columnNumber;
	}
	
	/**
	 * @return a new instance of matrix content
	 */
	public float[] getContent() {
		return content.clone();
	}

	/**
	 * @param content matrix content to set
	 */
	public void setContent(float[] content) {
		this.content = content;
	}

	/**
	 * @return sum of the diagonal
	 */
	public float trace() {
		float result=0;
		for (int i = 0; i < content.length; i+=columnNumber+1) {
			result+=content[i];
		}
		return result;
	}

	/**
	 * @return sub on the diagonal
	 */
	public LinearMatrix getSubIdentity() {
		LinearMatrix mat=new LinearMatrix(getContent(), getColumnNumber());
		float [] content=mat.content;
		int columns = columnNumber;
		MatrixToolkit.subIdentity(content, columns);
		return mat;
	}

	/**
	 * @return size of matrix content, row*columns
	 */
	public int size() {
		return content.length;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public LinearMatrix clone() throws CloneNotSupportedException {
		return new LinearMatrix(content.clone(), columnNumber);
	}
}
