package it.insubria.cabAlgorithm.dataStructures;

import com.amd.aparapi.Kernel;
import com.amd.aparapi.Kernel.EXECUTION_MODE;

/**
 * @author Andrea Marisio<br>
 *this class made operation on arrays like it is a linear matrix or vector, can be executed on CPU or GPU with aparapi, but the last one is slow need to be improved
 */
public class MatrixToolkit {
	/**
	 * 
	 */
	private static final Kernel.EXECUTION_MODE _executionMode = EXECUTION_MODE.CPU;
	/**
	 * @param real
	 * @param content
	 * @return
	 */
	public static float[] getSubArray(float real, float[] content) {
		return getSumArray(-real, content);
	}
	
	/**
	 * @param real
	 * @param content
	 * @return
	 */
	public static float[] getSumArray(float real, float[] content) {
		float[] data=content.clone();
		sumArray( real, data);
		return data;
	} 
	
	/**
	 * @param real
	 * @param content
	 */
	public static void subArray(float real, float[] content) {
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			sumArrayGPU(-real, content);
		} else {
			sumArrayCPU(-real, content);
		}
	}
	/**
	 * @param real
	 * @param content
	 */
	public static void sumArray(float real, float[] content) {
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			sumArrayGPU(real, content);
		} else {
			sumArrayCPU(real, content);
		}
	}

	/**
	 * @param real
	 * @param content
	 */
	private static void sumArrayCPU(float real, float[] content) {
		for (int k = 0; k < content.length; k++) {
			content[k] = content[k] + real;
		}
	}

	/**
	 * @param real
	 * @param content
	 */
	private static void sumArrayGPU(final float real, final float[] content) {
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				content[k] = content[k] + real;
			}
		};

		kernel.execute(content.length);
		kernel.dispose();
	}
	// -------------------multArray&divArray for a constant-----------------

	/**
	 * @param real
	 * @param content
	 * @return
	 */
	public static float[] getMultArray(float real, float[] content) {
		float[] data;
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			data = getMultArrayGPU(real, content);
		} else {
			data = getMultArrayCPU(real, content);
		}
		return data;
	}

	/**
	 * @param real
	 * @param content
	 * @return
	 */
	private static float[] getMultArrayCPU(float real, float[] content) {
		float[] data = new float[content.length];
		for (int k = 0; k < content.length; k++) {
			data[k] = content[k] * real;
		}
		return data;
	}

	/**
	 * @param real
	 * @param content
	 * @return
	 */
	private static float[] getMultArrayGPU(final float real, final float[] content) {
		final float[] data = new float[content.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				data[k] = content[k] * real;
			}
		};

		kernel.execute(content.length);
		kernel.dispose();
		return data;
	}

	/**
	 * @param real
	 * @param content
	 * @return
	 */
	public static float[] getDivArray(float real, float[] content) {
		float[] data;
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			data = getDivArrayGPU(real, content);
		} else {
			data = getDivArrayCPU(real, content);
		}
		return data;
	}

	/**
	 * @param real
	 * @param content
	 * @return
	 */
	private static float[] getDivArrayCPU(float real, float[] content) {
		float[] data = new float[content.length];
		for (int k = 0; k < content.length; k++) {
			data[k] = content[k] / real;
		}
		return data;
	}

	/**
	 * @param real
	 * @param content
	 * @return
	 */
	private static float[] getDivArrayGPU(final float real, final float[] content) {
		final float[] data = new float[content.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				data[k] = content[k] / real;
			}
		};

		kernel.execute(content.length);
		kernel.dispose();
		return data;
	}
	// ------------------------multArray&divArray with an array by
	// component-----------------

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	public static float[] getMultArrayByComponent(float[] contentA, float[] contentB) {
		float[] result;
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			result = getMultArrayByComponentGPU(contentA, contentB);
		} else {
			result = getMultArrayByComponentCPU(contentA, contentB);
		}
		return result;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getMultArrayByComponentCPU(float[] contentA, float[] contentB) {
		float[] result = new float[contentA.length];
		for (int k = 0; k < result.length; k++) {
			result[k] = contentA[k] * contentB[k];

		}
		return result;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getMultArrayByComponentGPU(final float[] contentA, final float[] contentB) {
		final float[] result = new float[contentA.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				result[k] = contentA[k] * contentB[k];
			}
		};
		kernel.execute(contentA.length);
		kernel.dispose();
		return result;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	public static float[] getDivArrayByComponent(float[] contentA, float[] contentB) {
		float[] result;
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			result = getDivArrayByComponentGPU(contentA, contentB);
		} else {
			result = getDivArrayByComponentCPU(contentA, contentB);
		}
		return result;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getDivArrayByComponentCPU(float[] contentA, float[] contentB) {
		float[] result = new float[contentA.length];
		for (int k = 0; k < result.length; k++) {
			result[k] = contentA[k] / contentB[k];

		}
		return result;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getDivArrayByComponentGPU(final float[] contentA, final float[] contentB) {
		final float[] result = new float[contentA.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				result[k] = contentA[k] / contentB[k];
			}
		};

		kernel.execute(contentA.length);
		kernel.dispose();
		return result;
	}
	// ------------------------getIdentity-----------------

	/**
	 * @param rows
	 * @return
	 */
	public static float[] getIdentity(int rows) {
		float[] data;
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			data = getIdentityGPU(rows);
		} else {
			data = getIdentityCPU(rows);
		}
		return data;
	}

	/**
	 * @param rows
	 * @return
	 */
	private static float[] getIdentityCPU(int rows) {
		float[] data = new float[rows * rows];
		for (int k = 0; k < rows; k++) {
			data[k * rows + k] = 1f;
		}
		return data;
	}

	/**
	 * @param rows
	 * @return
	 */
	private static float[] getIdentityGPU(final int rows) {
		final float[] data = new float[rows * rows];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				data[k * rows + k] = 1f;
			}
		};

		kernel.execute(rows);
		kernel.dispose();
		return data;
	}
	// ------------------------subIdentity-----------------

	/**
	 * @param content
	 * @param columns
	 */
	public static void subIdentity(float[] content, int columns) {
		if (_executionMode.equals(EXECUTION_MODE.GPU)) {
			subIdentityGPU(content, columns);
		} else {
			subIdentityCPU(content, columns);
		}
	}

	/**
	 * @param content
	 * @param columns
	 */
	private static void subIdentityCPU(float[] content, int columns) {
		for (int k = 0; k < columns; k++) {
			content[k * columns + k]--;
		}

	}

	/**
	 * @param content
	 * @param columns
	 */
	private static void subIdentityGPU(final float[] content, final int columns) {
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				content[k * columns + k]--;
			}
		};

		kernel.execute(columns);
		kernel.dispose();
	}
	

	// ------------------------getArrayReciprocal-----------------

	/**
	 * @param content
	 * @return
	 */
	public static float[] getArrayReciprocal(float[] content) {
		float[] result;
		if (_executionMode == EXECUTION_MODE.GPU) {
			result = getArrayReciprocalGPU(content);
		} else {
			result = getArrayReciprocalCPU(content);
		}
		return result;
	}

	/**
	 * @param content
	 * @return
	 */
	private static float[] getArrayReciprocalCPU(float[] content) {
		float[] reciprocal = new float[content.length];
		for (int k = 0; k < reciprocal.length; k++) {
			reciprocal[k] = 1f / content[k];
		}
		return reciprocal;
	}

	/**
	 * @param content
	 * @return
	 */
	private static float[] getArrayReciprocalGPU(final float[] content) {
		final float[] reciprocal = new float[content.length];
		int rowOrColumnNumber = content.length;
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				reciprocal[k] = 1f / content[k];
			}
		};

		kernel.execute(rowOrColumnNumber);
		kernel.dispose();
		return reciprocal;
	}

	/**
	 * @param rowOrColumnNumber
	 * @param f
	 * @return
	 */
	public static float[] getInizializedArray(int rowOrColumnNumber, float f) {
		float[] result;
		if (_executionMode == EXECUTION_MODE.GPU) {
			result = getInizialedArrayGPU(rowOrColumnNumber, f);
		} else {
			result = getInizialedArrayCPU(rowOrColumnNumber, f);
		}
		return result;
	}

	/**
	 * @param rowOrColumnNumber
	 * @param f
	 * @return
	 */
	private static float[] getInizialedArrayCPU(int rowOrColumnNumber, float f) {
		float[] inizialized = new float[rowOrColumnNumber];
		for (int k = 0; k < inizialized.length; k++) {
			inizialized[k] = f;
		}
		return inizialized;
	}

	/**
	 * @param rowOrColumnNumber
	 * @param f
	 * @return
	 */
	private static float[] getInizialedArrayGPU(int rowOrColumnNumber, final float f) {
		final float[] inizialized = new float[rowOrColumnNumber];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				inizialized[k] = f;
			}
		};

		kernel.execute(rowOrColumnNumber);
		kernel.dispose();
		return inizialized;
	}

	// ------------------------SUM-----------------
	
	
	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	public static float[] getSumArrayByComponent(final float[] contentA, final float[] contentB) {
		float[] data;
		if (_executionMode == EXECUTION_MODE.CPU) {
			data = getSumArrayByComponentCPU(contentA, contentB);
		} else {
			data = getSumArrayByComponentGPU(contentA, contentB);
		}
		return data;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getSumArrayByComponentCPU(float[] contentA, float[] contentB) {
		float[] data = new float[contentA.length];
		for (int k = 0; k < contentA.length; k++) {
			data[k] = contentA[k] + contentB[k];
			}
		return data;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getSumArrayByComponentGPU(final float[] contentA, final float[] contentB) {
		final float[] data = new float[contentA.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				data[k] = contentA[k] + contentB[k];
			}
		};

		kernel.execute(contentA.length);
		kernel.dispose();
		return data;
	}

	// ------------------------SUB-----------------
	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	public static float[] getSubArrayByComponent(final float[] contentA, final float[] contentB) {
		if (contentA.length != contentB.length) {
			throw new IllegalArgumentException("size of first argument mismatch with second argument's one");
		}
		float[] data;
		if (_executionMode == EXECUTION_MODE.GPU) {
			data = getSubArrayByComponentGPU(contentA, contentB);
		} else {
			data = getSubArrayByComponentCPU(contentA, contentB);

		}
		return data;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getSubArrayByComponentCPU(float[] contentA, float[] contentB) {
		float[] data = new float[contentA.length];
		for (int k = 0; k < contentA.length; k++) {
			data[k] = contentA[k] - contentB[k];
		}
		return data;
	}

	/**
	 * @param contentA
	 * @param contentB
	 * @return
	 */
	private static float[] getSubArrayByComponentGPU(final float[] contentA, final float[] contentB) {
		final float[] data = new float[contentA.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				data[k] = contentA[k] - contentB[k];
			}
		};

		kernel.execute(contentA.length);
		kernel.dispose();
		return data;
	}

	// ------------------------mult-----------------
	/**
	 * @param A
	 * @param B
	 * @param columnOfA
	 * @param columnOfB
	 * @return
	 */
	public static float[] getMatrixMultiplication(final float[] A, final float[] B, final int columnOfA, final int columnOfB) {
		int rowOfB = B.length / columnOfB;
		if (rowOfB != columnOfA) {
			throw new IllegalArgumentException("column of first argument mismatch with row of second argument");
		}
		int rowOfA = A.length / columnOfA;
		float[] result;
		if (_executionMode == EXECUTION_MODE.GPU) {
			result = getMatrixMultiplicationGPU(A, B, columnOfA, columnOfB, rowOfA);
		} else {
			result = getMatrixMultiplicationCPU(A, B, columnOfA, columnOfB, rowOfA);
		}
		return result;
	}

	/**
	 * @param A
	 * @param B
	 * @param columnOfA
	 * @param columnOfB
	 * @param rowOfA
	 * @return
	 */
	private static float[] getMatrixMultiplicationCPU(float[] A, float[] B, int columnOfA, int columnOfB, int rowOfA) {
		float[] result = new float[rowOfA * columnOfB];
		int line;
		int column;
		float value;
		for (int k = 0; k < result.length; k++) {
			line = k / columnOfB;
			column = k % columnOfB;
			value = 0;
			for (int i = 0; i < columnOfA; i++) {
				value =value+ A[line * columnOfA + i] * B[i * columnOfB + column];
			}
			result[k] = value;
		}
		
		return result;
	}

	/**
	 * @param A
	 * @param B
	 * @param columnOfA
	 * @param columnOfB
	 * @param rowOfA
	 * @return
	 */
	private static float[] getMatrixMultiplicationGPU(final float[] A, final float[] B, final int columnOfA, final int columnOfB,
			int rowOfA) {
		final float[] result = new float[rowOfA * columnOfB];

		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				int line = k / columnOfB;
				int column = k % columnOfB;
				float value = 0;
				for (int i = 0; i < columnOfA; i++) {
					value += A[line * columnOfA + i] * B[i * columnOfB + column];
				}
				result[k] = value;
			}
		};

		kernel.execute(result.length);
		kernel.dispose();
		return result;
	}
	// ------------------------getTransposeMatrix-----------------

	/**
	 * @param content
	 * @param columnsNumber
	 * @return
	 */
	public static float[] getTransposeMatrix(float[] content, int columnsNumber) {
		float[] data;
		if (_executionMode == EXECUTION_MODE.GPU) {
			data = getTransposeMatrixGPU(content, columnsNumber);
		} else {
			data = getTransposeMatrixCPU(content, columnsNumber);
		}
		return data;
	}

	/**
	 * @param content
	 * @param columnsNumber
	 * @return
	 */
	private static float[] getTransposeMatrixCPU(float[] content, int columnsNumber) {
		float[] data = new float[content.length];
		int c;
		int column;
		for (int k = 0; k < data.length; k++) {
			c = k % columnsNumber;
			column = k / columnsNumber;
			data[k] = content[c * columnsNumber + column];
		}
		return data;
	}

	/**
	 * @param content
	 * @param columnsNumber
	 * @return
	 */
	private static float[] getTransposeMatrixGPU(final float[] content, final int columnsNumber) {
		final float[] data = new float[content.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				int c = k % columnsNumber;
				int column = k / columnsNumber;
				data[k] = content[c * columnsNumber + column];
			}
		};
		kernel.execute(content.length);
		kernel.dispose();
		return data;
	}
	// ------------------------vectorsOperations toSquareMatrix-----------------

	/**
	 * @param cont
	 * @param dataColumn
	 * @return
	 */
	public static float[] toSquareMatrix(float[] cont, int dataColumn) {
		float[] data;
		if (_executionMode == EXECUTION_MODE.GPU) {
			data = toSquareMatrixGPU(cont, dataColumn);
		} else {
			data = toSquareMatrixCPU(cont, dataColumn);
		}
		return data;
	}

	/**
	 * @param cont
	 * @param dataColumn
	 * @return
	 */
	private static float[] toSquareMatrixCPU(float[] cont, int dataColumn) {
		float[] data = new float[dataColumn * dataColumn];
		int line;
		int column;
		for (int k = 0; k < data.length; k++) {
			line = k / dataColumn;
			column = k % dataColumn;
			data[k] = cont[line] * cont[column];
		}
		return data;
	}

	/**
	 * @param cont
	 * @param dataColumn
	 * @return
	 */
	private static float[] toSquareMatrixGPU(final float[] cont, final int dataColumn) {
		final float[] data = new float[dataColumn * dataColumn];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				int line = k / dataColumn;
				int column = k % dataColumn;
				data[k] = cont[line] * cont[column];
			}
		};
		kernel.execute(data.length);
		kernel.dispose();
		return data;
	}
	// ------------------------vectorsOperations dotProduct-----------------

	/**
	 * @param row
	 * @param column
	 * @return
	 */
	public static float dotProduct(float[] row, float[] column) {
		if (row.length != column.length) {
			throw new IllegalArgumentException("column of first argument mismatch with row of second argument");
		}
		float result = 0;
		float[] partialResult;
		
		if (_executionMode == EXECUTION_MODE.GPU) {
			partialResult = dotProductGPU(row, column);
		} else {
			partialResult = dotProductCPU(row, column);
		}
		for (float i : partialResult) {
			// System.out.println(i);//DEBUG
			result = result + i;
		}
		return result;
	}

	/**
	 * @param row
	 * @param column
	 * @return
	 */
	private static float[] dotProductCPU(float[] row, float[] column) {
		float[] partialResult = new float[row.length];
		for (int k = 0; k < partialResult.length; k++) {
			partialResult[k] = row[k] * column[k];
		}
		return partialResult;
		
	}

	/**
	 * @param row
	 * @param column
	 * @return
	 */
	private static float[] dotProductGPU(final float[] row, final float[] column) {
		final float[] partialResult = new float[row.length];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				partialResult[k] = row[k] * column[k];
			}
		};
		kernel.execute(row.length);
		kernel.dispose();
		return partialResult;
	}
	
	/**
	 * @param m
	 * @param columns
	 * @return
	 */
	public static String matrixToString(float[] m, int columns) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < m.length / columns; i++) {
			for (int j = 0; j < columns; j++) {
				b.append(m[i * columns + j] + " ");
			}
			b.append("\n");
		}
		return b.toString();
	}
	
	/**
	 * @param m
	 * @param columns
	 * @return
	 */
	public static String arrayToString(Object[] m, int columns) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < m.length / columns; i++) {
			for (int j = 0; j < columns; j++) {
				b.append(m[i * columns + j].toString() + " ");
			}
			b.append("\n");
		}
		return b.toString();
	}


	// --------------------invertSqareMatrix(too long-> last)-----------------

	/**
	 * @param matrix
	 * @param columnNumber
	 * @return
	 */
	public static float[] invertSqareMatrix(final float matrix[], int columnNumber) {
		int n = columnNumber;
		int dataLength = matrix.length;
		float result[] = new float[dataLength];
		float b[][] = new float[n][n];
		int index[] = new int[columnNumber];
		for (int i = 0; i < n; ++i) {
			b[i][i] = 1;
		}
		// Transform the matrix into an upper triangle
		final float[] matrixToInvert = new float[dataLength];
		Kernel kernel = new Kernel() {
			@Override
			public void run() {
				int k = getGlobalId();
				matrixToInvert[k] = matrix[k];
			}
		};
		kernel.execute(result.length);
		kernel.dispose();
		gaussian(matrixToInvert, columnNumber, index);

		// Update the matrix b[i][j] with the ratios stored
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					b[index[j]][k] -= matrixToInvert[index[j] * columnNumber + i] * b[index[i]][k];
				}
			}
		}
		// Perform backward substitutions
		for (int i = 0; i < n; ++i) {
			result[(n - 1) * columnNumber + i] = b[index[n - 1]][i]
					/ matrixToInvert[index[n - 1] * columnNumber + (n - 1)];
			for (int j = n - 2; j >= 0; --j) {
				result[j * columnNumber + i] = b[index[j]][i];
				for (int k = j + 1; k < n; ++k) {
					result[j * columnNumber + i] -= matrixToInvert[index[j] * columnNumber + k]
							* result[k * columnNumber + i];
				}
				result[j * columnNumber + i] /= matrixToInvert[index[j] * columnNumber + j];
			}
		}
		return result;
	}

	// Method to carry out the partial-pivoting Gaussian
	// elimination. Here index[] stores pivoting order.
	/**
	 * @param matrixToResolve
	 * @param columnNumber
	 * @param index
	 */
	public static void gaussian(float matrixToResolve[], int columnNumber, int index[]) {
		int n = index.length;
		float c[] = new float[n];
		// Initialize the index
		for (int i = 0; i < n; ++i) {
			index[i] = i;
		}
		// Find the rescaling factors, one from each row
		for (int i = 0; i < columnNumber; ++i) {
			float c1 = 0;
			for (int j = 0; j < columnNumber; ++j) {
				float c0 = Math.abs(matrixToResolve[i * columnNumber + j]);
				if (c0 > c1) {
					c1 = c0;
				}
			}
			c[i] = c1;
		}
		// Search the pivoting element from each column
		int k = 0;
		for (int j = 0; j < n - 1; ++j) {
			float pi1 = 0;
			for (int i = j; i < n; ++i) {
				float pi0 = Math.abs(matrixToResolve[index[i] * columnNumber + j]);
				pi0 /= c[index[i]];

				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}
			// Interchange rows according to the pivoting order
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;

			for (int i = j + 1; i < n; ++i) {
				float pj = matrixToResolve[index[i] * columnNumber + j] / matrixToResolve[index[j] * columnNumber + j];
				// Record pivoting ratios below the diagonal
				matrixToResolve[index[i] * columnNumber + j] = pj;
				// Modify other elements accordingly
				for (int l = j + 1; l < n; ++l) {
					matrixToResolve[index[i] * columnNumber + l] -= pj * matrixToResolve[index[j] * columnNumber + l];
				}
			}
		}
	}

}
