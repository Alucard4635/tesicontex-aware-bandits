package it.insubria.cabAlgorithm.dataStructures;

/**
 * @author Andrea Marisio <br>
 *
 * @param <RappresentationType> type of representation of features
 */
public interface Advisable<RappresentationType>  {
	/**pass the reference of features data structures,for improve performance 
	 * @return the <u>reference</u> data structure of features
	 */
	public RappresentationType features();
	/**set content of advisable
	 * @param features content of features
	 */
	public void setFeatures(RappresentationType features) ;
	/**
	 * @return a unique identificative int
	 */
	public int hashCode();
	
	/**classic {@link Object} equals object, requested for a correct use of hashset and hashmap
	 * @param obj
	 * @return
	 */
	@Override
	public boolean equals(Object obj);

}
