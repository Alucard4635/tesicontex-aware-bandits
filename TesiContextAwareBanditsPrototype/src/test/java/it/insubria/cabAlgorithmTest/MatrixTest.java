package it.insubria.cabAlgorithmTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import it.insubria.cabAlgorithm.dataStructures.LinearMatrix;
import it.insubria.cabAlgorithm.dataStructures.MatrixToolkit;
import it.insubria.cabAlgorithm.dataStructures.Vector;

public class MatrixTest {

	@Test
	public void identityTest() {
		float[][] mat2 = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		int nCol = mat2[0].length;
		float[] mat = new float[nCol * nCol];
		convertToOneDimensionalArray(mat2, nCol, mat);
		LinearMatrix ident = LinearMatrix.getIdentity(nCol);
		float[] content = ident.getContent();
		for (int i = 0; i < mat.length; i++) {
			assertEquals(mat[i], content[i], 0);
		}
	}

	@Test
	public void subTest() {
		int ncol = 5;
		LinearMatrix res = new LinearMatrix(ncol)
				.getSub(new LinearMatrix(ncol));
		float[] content = res.getContent();
		for (int i = 0; i < ncol; i++) {
			assertEquals(0.0, content[i], 0);
		}
	}

	@Test
	public void multTest() {
		float[] matrixA = { 2, 5, 
							33, 7,
							65, -12};
		float[] matrixB = { 2,5,33,
							7,65,-12};
		float[] mult = MatrixToolkit.getMatrixMultiplication(matrixA, matrixB, 2, 3);
		float[] result = {	39,335, 6,
							115, 620, 1005,
							46, -455, 2289 };
		for (int i = 0; i < result.length; i++) {
			assertEquals(result[i], mult[i], 0);
		}
	}

	@Test
	public void sumTest() {
		float[][] mat2 = { { 1, 8, 5 },
							{ 0, 2, 2 }, 
							{ 0, 5, 2 } };
		int nCol = mat2[0].length;
		float[] mat = new float[nCol * nCol];
		convertToOneDimensionalArray(mat2, nCol, mat);
		float[] matrix = MatrixToolkit.getSumArrayByComponent(mat, mat);
		for (int i = 0; i < mat2.length; i++) {
			for (int j = 0; j < mat2[i].length; j++) {
				mat[i * nCol + j] += mat2[i][j];
			}
		}
		for (int i = 0; i < matrix.length; i++) {
			assertEquals(mat[i], matrix[i], 0);
		}
	}

	@Test
	public void gpuInvertTest() {
		float[][] mat2 = { { 5, 0, 0, 5 },
							{ 0, 5, 5, 5 },
							{ 0, 0, 5, 5 },
							{ 4, 0, 0, 5 } };
		int nCol = mat2[0].length;
		float[] mat = new float[nCol * nCol];
		convertToOneDimensionalArray(mat2, nCol, mat);
		assertEquals(true, det(mat2, nCol)!=0);

		float[] invertSqareMatrix = LinearMatrix.invertSqareMatrix(mat,
				nCol);
		String matrixToString = MatrixToolkit.matrixToString(
				invertSqareMatrix, nCol);
		float[][] invertORIGINAL = invertORIGINAL(mat2);
		
		assertEquals(matrixToString, matrixToString(invertORIGINAL));

		float[] identity = new LinearMatrix(nCol).getContent();
		float[] calculatedIdentity = MatrixToolkit
				.getMatrixMultiplication(mat, invertSqareMatrix, nCol, nCol);

		for (int i = 0; i < invertSqareMatrix.length; i++) {
			assertEquals(identity[i], calculatedIdentity[i], 0.0000001);
		}
	}

	@Test
	public void invertShermanMorrison() {
		float[][] mat1 = { 	{ 2, 2, 2, 2 },
							{ 0, 2, 0, 2 },
							{ 0, 0, 2, 2 },
							{ 0, 0, 0, 2 } };

		float[] vector = { 1, 2, 2, 1 };
		int nCol = mat1[0].length;
		
		float[] lMat1 = new float[nCol * nCol];
		convertToOneDimensionalArray(mat1, nCol, lMat1);
		assertEquals(true, det(mat1, nCol)!=0);
		assertEquals(det(mat1, nCol), det(lMat1, nCol),0);

		
		LinearMatrix m1 = new LinearMatrix(lMat1, nCol);
		Vector v = new Vector(vector);
		LinearMatrix squareMatrix = v.toSquareMatrix();
		LinearMatrix m2 = m1.getSum(squareMatrix);


		assertEquals(true, det(m2.getContent(), m2.getColumnNumber())!=0);
		assertEquals(true, det(m1.getContent(), m1.getColumnNumber())!=0);
		

		LinearMatrix m1Inverted = m1.getInverse();
		m1Inverted.shermanMorrison(v);
		LinearMatrix m2Inverted = m2.getInverse();
//		System.out.println("M1^-1:\n"+m1Inverted);
//		System.out.println("M2^-1:\n"+m2Inverted);
		
		LinearMatrix identiryMatrixFromM1 = m2.getMult(m1Inverted);
		float[] identityCalculatedFromM1 = identiryMatrixFromM1.getContent();
//		System.out.println("M1 identity:\n"+identiryMatrixFromM1);
		
		LinearMatrix identiryMatrixFromM2 = m2.getMult(m2Inverted);
//		System.out.println("M2 identity:\n"+identiryMatrixFromM2);

		float[] identityCalculatedFromM2 = identiryMatrixFromM2.getContent();
		
		float[] identity = LinearMatrix.getIdentity(nCol).getContent();
		
		float[] contentm1 = m1Inverted.getContent();
		float[] contentm2 = m2Inverted.getContent();
		for (int i = 0; i < contentm1.length; i++) {
			assertEquals(contentm2[i], contentm1[i], 0.00001);
		}
		for (int i = 0; i < identity.length; i++) {
			assertEquals(identity[i], identityCalculatedFromM2[i], 0.000001);
			assertEquals(identity[i], identityCalculatedFromM1[i], 0.000001);
		}

	}

	@Test
	public void multWithAVector() {
		float[][] mat1 ={ 	{ 2, 2, 2 },
							{ 0, 2, 2 },
							{ 0, 0, 2 } };
		float[] vector = { 1, 2, 3 };
		int nCol = mat1[0].length;
		float[] lMat1 = new float[nCol * nCol];
		convertToOneDimensionalArray(mat1, nCol, lMat1);
		LinearMatrix res = new LinearMatrix(lMat1, nCol)
				.getMult(new Vector(vector));
		assertEquals(1, res.getColumnNumber());
	}

	public void convertToOneDimensionalArray(float[][] mat1, int nCol, float[] lMat1) {
		for (int i = 0; i < mat1.length; i++) {
			for (int j = 0; j < mat1[i].length; j++) {
				lMat1[i * nCol + j] = mat1[i][j];
			}
		}
		
	}

	@Test
	public void traceTest() {
		float[][] mat1 = { { 2, 2, 2 }, { 0, 2, 2 }, { 0, 0, 2 } };
		int nCol = mat1[0].length;
		float[] lMat1 = new float[nCol * nCol];
		convertToOneDimensionalArray(mat1, nCol, lMat1);
		assertEquals(6, new LinearMatrix(lMat1, nCol).trace(), 0);
	}

	public static String matrixToString(float[][] m) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[0].length; j++) {
				b.append(m[i][j] + " ");
			}
			b.append("\n");
		}
		return b.toString();
	}

	private float det(float ai[][], int dimension) {
		float l = 0L;
		if (dimension == 1)
			l = ai[0][0];
		else if (dimension == 2) {
			l = ai[0][0] * ai[1][1] - ai[0][1] * ai[1][0];
		} else {
			float ai1[][] = new float[dimension - 1][dimension - 1];
			for (int k = 0; k < dimension; k++) {
				for (int i1 = 1; i1 < dimension; i1++) {
					int j = 0;
					for (int j1 = 0; j1 < dimension; j1++)
						if (j1 != k) {
							ai1[i1 - 1][j] = ai[i1][j1];
							j++;
						}

				}

				if (k % 2 == 0)
					l +=  ai[0][k] * det(ai1, dimension - 1);
				else
					l -=  ai[0][k] * det(ai1, dimension - 1);
			}

		}
		return l;
	}
	
	private float det(float ai[], int columnsNumber) {
		float l = 0L;
		if (ai.length/columnsNumber==columnsNumber&&ai.length%columnsNumber==0) {	
			if (columnsNumber == 1)
				l = ai[0];
			else if (columnsNumber == 2) {
				l = ai[0] * ai[3] - ai[1] * ai[2];
			} else {
				int newColumnsNumber = columnsNumber - 1;
				float ai1[] = new float[ai.length-columnsNumber-columnsNumber+1];
				for (int indexOnTheFirstRow = 0; indexOnTheFirstRow < columnsNumber; indexOnTheFirstRow++) {
					for (int i1 = 1; i1 < columnsNumber; i1++) {
						
						for (int j = 0, j1 = 0; j1 < columnsNumber; j1++)
							if (j1 != indexOnTheFirstRow) {
								ai1[(i1-1)*newColumnsNumber +j] =
										ai[i1*columnsNumber+j1];
								j++;
							}
	
					}
	
					if (indexOnTheFirstRow % 2 == 0)
						l +=  ai[indexOnTheFirstRow] * det(ai1, columnsNumber - 1);
					else
						l -=  ai[indexOnTheFirstRow] * det(ai1, columnsNumber - 1);
				}
	
			}
		}
		return l;
	}
	

	public static float[][] invertORIGINAL(final float matrix[][]) {
		int n = matrix.length;
		float result[][] = new float[n][n];
		float b[][] = new float[n][n];
		int index[] = new int[n];
		for (int i = 0; i < n; ++i){
			b[i][i] = 1;
		}
		
		float mat[][] = new float[n][n];
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				mat[i][j]=matrix[i][j];
			}
		}
		// Transform the matrix into an upper triangle
		gaussianORIGINAL(mat, index);
		/*
		 * for (int i = 0; i < index.length; i++) {
		 * System.out.print(index[i]+" "); }System.out.println();
		 */

		// Update the matrix b[i][j] with the ratios stored
		for (int i = 0; i < n - 1; ++i) {
			for (int j = i + 1; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					b[index[j]][k] -= mat[index[j]][i] * b[index[i]][k];
				}
			}
		}
		// System.out.println(matrixToString(b));

		// Perform backward substitutions
		for (int i = 0; i < n; ++i) {
			result[n - 1][i] = b[index[n - 1]][i] / mat[index[n - 1]][n - 1];
			for (int j = n - 2; j >= 0; --j) {
				result[j][i] = b[index[j]][i];
				for (int k = j + 1; k < n; ++k) {
					result[j][i] -= mat[index[j]][k] * result[k][i];
				}
				result[j][i] /= mat[index[j]][j];
			}
		}

		return result;
	}

	// Method to carry out the partial-pivoting Gaussian
	// elimination. Here index[] stores pivoting order.
	public static void gaussianORIGINAL(float matrix[][], int index[]) {
		int n = index.length;
		float c[] = new float[n];
		// Initialize the index
		for (int i = 0; i < n; ++i)
			index[i] = i;
		// Find the rescaling factors, one from each row
		for (int i = 0; i < n; ++i) {
			float c1 = 0;
			for (int j = 0; j < n; ++j) {
				float c0 = Math.abs(matrix[i][j]);
				if (c0 > c1)
					c1 = c0;
			}
			c[i] = c1;
		}

		// Search the pivoting element from each column
		int k = 0;
		for (int j = 0; j < n - 1; ++j) {
			float pi1 = 0;
			for (int i = j; i < n; ++i) {
				float pi0 = Math.abs(matrix[index[i]][j]);
				pi0 /= c[index[i]];
				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}
			// Interchange rows according to the pivoting order
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;
			for (int i = j + 1; i < n; ++i) {
				float pj = matrix[index[i]][j] / matrix[index[j]][j];
				// Record pivoting ratios below the diagonal
				matrix[index[i]][j] = pj;
				// Modify other elements accordingly
				for (int l = j + 1; l < n; ++l) {
					matrix[index[i]][l] -= pj * matrix[index[j]][l];
				}
			}
		}
	}
}
