package it.insubria.cabAlgorithmTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import it.insubria.cabAlgorithm.dataStructures.LinearMatrix;
import it.insubria.cabAlgorithm.dataStructures.Vector;

public class VectorTest {
	@Test
	public void toMatrixTest() {
		float[] data={1,2,3,4,5,6,7,8,9,10};
		float[] result = new Vector(data).toSquareMatrix().getContent();
		float row=0;
		for (int i = 0; i < result.length; i++) {
			float column = (i)%data.length+1;
			if (i%data.length==0) {
				row++;
			}
			assertEquals(column*row,result[i],0);
		}
	}

	@Test
	public void toMatrixTest2() {
		float[] data={123,54,789,-654,-78,456};
		float[] result = new Vector(data).toSquareMatrix().getContent();
		for (int i = 0; i < result.length; i++) {
			assertEquals(0,result[i]%data[i/data.length],0);
			assertEquals(0,result[i]%data[i%data.length],0);

		}
	}
}
