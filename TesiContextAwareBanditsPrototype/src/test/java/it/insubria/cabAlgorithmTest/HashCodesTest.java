package it.insubria.cabAlgorithmTest;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import it.insubria.cabAlgorithm.dataStructures.FeaturesAdvisable;
import it.insubria.cabAlgorithm.dataStructures.OnAxisAdvisable;
import it.insubria.cabAlgorithm.dataStructures.Seeker;
import it.insubria.cabAlgorithm.dataStructures.SimplifiedSeeker;

public class HashCodesTest {

	@Test
	public void OnAxisAdvisableTest(){
		HashMap<OnAxisAdvisable, String> map=new HashMap<OnAxisAdvisable, String>();
		int iterations=10;
		for (int i = 0; i < iterations; i++) {
			OnAxisAdvisable advisable = new OnAxisAdvisable(i);
			map.put(advisable, "ad"+i);
		}
		assertEquals(iterations,map.size(),0);
		for (int i = 0; i < iterations; i++) {
			OnAxisAdvisable advisable = new OnAxisAdvisable(i);
			String string = map.get(advisable);
			assertEquals("ad"+i, string);
		}
	}
	@Test
	public void FeaturesAdvisableTest(){
		HashMap<FeaturesAdvisable, String> map=new HashMap<FeaturesAdvisable, String>();
		int iterations=10;
		for (int i = 0; i < iterations; i++) {
			String id= "ad"+i;

			FeaturesAdvisable advisable = new FeaturesAdvisable(i,id);
			map.put(advisable, id);
		}
		assertEquals(iterations,map.size(),0);
		for (int i = 0; i < iterations; i++) {
			String id= "ad"+i;
			FeaturesAdvisable advisable = new FeaturesAdvisable(i,id);
			String string = map.get(advisable);
			assertEquals("ad"+i, string);
		}
	}
	
	@Test
	public void SimplifiedSeekerTest(){
		HashMap<SimplifiedSeeker, String> map=new HashMap<SimplifiedSeeker, String>();
		int iterations=10;
		for (int i = 0; i < iterations; i++) {
			String id= "ad"+i;

			SimplifiedSeeker advisable = new SimplifiedSeeker(i,id);
			map.put(advisable, id);
		}
		assertEquals(iterations,map.size(),0);
		for (int i = 0; i < iterations; i++) {
			String id= "ad"+i;
			SimplifiedSeeker advisable = new SimplifiedSeeker(i,id);
			String string = map.get(advisable);
			assertEquals("ad"+i, string);
		}
	}
	
	@Test
	public void SeekerTest(){
		HashMap<Seeker, String> map=new HashMap<Seeker, String>();
		int iterations=10;
		for (int i = 0; i < iterations; i++) {
			String id= "ad"+i;

			Seeker advisable = new Seeker(i,id);
			map.put(advisable, id);
		}
		assertEquals(iterations,map.size(),0);
		for (int i = 0; i < iterations; i++) {
			String id= "ad"+i;
			Seeker advisable = new Seeker(i,id);
			String string = map.get(advisable);
			assertEquals("ad"+i, string);
		}
	}
}
