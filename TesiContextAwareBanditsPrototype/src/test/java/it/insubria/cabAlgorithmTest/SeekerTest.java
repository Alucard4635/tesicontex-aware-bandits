package it.insubria.cabAlgorithmTest;

import static org.junit.Assert.*;
import org.junit.Test;

import it.insubria.AlgorithmEvaluator.ConsulterEvauator;
import it.insubria.cabAlgorithm.AdvisableConsulter;
import it.insubria.cabAlgorithm.dataStructures.FeaturesAdvisable;
import it.insubria.cabAlgorithm.dataStructures.LinearMatrix;
import it.insubria.cabAlgorithm.dataStructures.MoldableModelNode;
import it.insubria.cabAlgorithm.dataStructures.Vector;
import it.insubria.socialNetwork.DirectionalLink;
import it.insubria.cabAlgorithm.dataStructures.Seeker;

public class SeekerTest {

	private static final int PROPAGATE_MAX_DISTANCE = 0;

	@Test
	public void addMemoryTest(){
		float[][] mat2 = { 
				{ 1, 0, 0 }, 
				{ 0, 1, 0 }, 
				{ 0, 0, 1 } };
		int nCol=mat2[0].length;
		float[] mat=new float[nCol*nCol];
		convertToOneDimensionalArray(mat2, nCol, mat);
		
		Seeker s=new Seeker(nCol);
		
		LinearMatrix memoryToAdd = new LinearMatrix(mat,nCol);
		float[] payoff=mat2[0];
		for (int i = 0; i < payoff.length; i++) {
			if (i==0) {
				payoff[i]=mat2[0][i]-1;
			}else {
				payoff[i]=mat2[0][i];
			}
		}
		Vector payoffVector = new Vector(payoff);
		//s.addMemory(memoryToAdd, payoffVector);
		float[] memoryContent = s.memory().getContent();
		float[] payoffContent=s.getPayoffModel().getContent();
		
		for (int i = 0; i < payoff.length; i++) {
			assertEquals(payoff[i],payoffContent[i],0);

		}
		for (int i = 0; i < mat.length; i++) {
				assertEquals(mat[i],memoryContent[i],0);
		}
		LinearMatrix inverse = memoryToAdd.getInverse();
		LinearMatrix mult = inverse.getMult(s.getPayoffModel());
		float[] content = mult.getContent();
		float[] modelContent = s.getModel().getContent();
		for (int i = 0; i < modelContent.length; i++) {
			assertEquals(content[i],modelContent[i],0);

		}
	
	}

	public void convertToOneDimensionalArray(float[][] mat2, int nCol, float[] mat) {
		for (int i = 0; i < mat2.length; i++) {
			for (int j = 0; j < mat2[i].length; j++) {
				mat[i*nCol+j]=mat2[i][j];
			}
		}
	}
	
	@Test
	public void relationsTest(){
		int payoff = 1;
		int multForRelations = 1;
		float[] features={5,6,1,7,5,2,8,9,3,1,5};
		int size = features.length;
		Seeker s=new Seeker(size);
		Seeker sNoRelations=new Seeker(size);
		if (PROPAGATE_MAX_DISTANCE>0) {
			s.addDirectionalLink(new DirectionalLink(s));
			multForRelations=2;
		}
		FeaturesAdvisable objectOffered = new FeaturesAdvisable(features,"-1");
		sNoRelations.update(payoff, objectOffered);
		s.update(payoff, objectOffered);
		
		float[] contentSNoRelations = sNoRelations.memory().getSubIdentity().getContent();
		float[] contentS = s.memory().getSubIdentity().getContent();

		for (int i = 0; i < contentS.length; i++) {
			assertEquals(contentSNoRelations[i]*multForRelations, contentS[i],0.000001);
		}
		

	}
	
	
}
