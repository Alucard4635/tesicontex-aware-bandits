package it.insubria.cabAlgorithmTest;

import static org.junit.Assert.*;

import org.junit.Test;

import it.insubria.cabAlgorithm.clusters.SimplifiedCABClusterMoldableModel;
import it.insubria.cabAlgorithm.clusters.ClusterMoldableModel;
import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.DiagonalMatrix;
import it.insubria.cabAlgorithm.dataStructures.FeaturesAdvisable;
import it.insubria.cabAlgorithm.dataStructures.LinearMatrix;
import it.insubria.cabAlgorithm.dataStructures.MatrixToolkit;
import it.insubria.cabAlgorithm.dataStructures.MoldableModel;
import it.insubria.cabAlgorithm.dataStructures.OnAxisAdvisable;
import it.insubria.cabAlgorithm.dataStructures.OnAxisVector;
import it.insubria.cabAlgorithm.dataStructures.Seeker;
import it.insubria.cabAlgorithm.dataStructures.SimplifiedSeeker;
import it.insubria.cabAlgorithm.dataStructures.Vector;

public class ClustersTest {
	@Test
	public void modelClusterSeekerTest(){
		int payoff = 5;
		int featuresSize = 3;
		float[] features=new float[featuresSize];
		for (int i = 0; i < features.length; i++) {
			features[i]=50;
		}
		Seeker model = new Seeker(featuresSize);
		
		ClusterMoldableModel<LinearMatrix, Vector> clusterMoldableModel = new SimplifiedCABClusterMoldableModel<LinearMatrix, Vector>(model);
			
		MoldableModel<LinearMatrix,Vector> clusterModel = clusterMoldableModel.getClusterModel();
		float[] content = model.memory().getContent();
		float[] contentCluster = clusterModel.memory().getContent();
		
		for (int i = 0; i < content.length; i++) {
			assertEquals(content[i],contentCluster[i],0.0000001);
		}
		clusterMoldableModel.add(model);
		content = model.memory().getContent();
		contentCluster = clusterModel.memory().getContent();
		for (int i = 0; i < contentCluster.length; i++) {
			assertEquals(content[i],contentCluster[i],0.0000001);
		}
		for (int x = 0; x < 10; x++) {
		model.update(payoff,new FeaturesAdvisable(features,"-1") );
		Seeker model2 = new Seeker(featuresSize);
		model2.update(payoff,new FeaturesAdvisable(features,"-1") );
		clusterMoldableModel.add(model);
		clusterMoldableModel.add(model2);

		content = model.memory().getContent();
		float[] content2 = model2.memory().getContent();

		contentCluster = clusterModel.memory().getContent();

		for (int i = 0; i < contentCluster.length; i++) {
			assertNotEquals(content[i],contentCluster[i]);
			assertNotEquals(content2[i],contentCluster[i]);

		}
		}
	}
	

	
	@Test
	public void modelClusterSimplifiedSeekerTest(){
		int payoff = 5;
		int featuresSize = 10;
		
		SimplifiedSeeker model = new SimplifiedSeeker(featuresSize, "1");
		
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel = new SimplifiedCABClusterMoldableModel<DiagonalMatrix, OnAxisVector>(model);
			
		MoldableModel<DiagonalMatrix, OnAxisVector> clusterModel = clusterMoldableModel.getClusterModel();
		float[] content = model.memory().getDiagonal().getContent();
		float[] contentCluster = clusterModel.memory().getDiagonal().getContent();
		
		for (int i = 0; i < content.length; i++) {
			assertEquals(content[i],contentCluster[i],0.0000001);
		}
		clusterMoldableModel.add(model);
		content = model.memory().getDiagonal().getContent();
		contentCluster = clusterModel.memory().getDiagonal().getContent();
		for (int i = 0; i < contentCluster.length; i++) {
			assertEquals(content[i],contentCluster[i],0.0000001);
		}
		SimplifiedSeeker model2 = new SimplifiedSeeker(featuresSize, "2");
		for (int x = 0; x < 10; x++) {
			Advisable<OnAxisVector> features=new OnAxisAdvisable(x);
			model.update(payoff,features );
			model2.update(payoff,features );
			clusterMoldableModel.add(model);
			clusterMoldableModel.add(model2);
	
			content = model.memory().getDiagonal().getContent();
			float[] content2 = model2.memory().getDiagonal().getContent();
	
			contentCluster = clusterModel.memory().getDiagonal().getContent();
			assertNotEquals(content[x],contentCluster[x]);
			assertNotEquals(content2[x],contentCluster[x]);
		}
		
	}
	
	@Test
	public void modelClusterSimplifiedSeeker2Test(){
		int payoff = 5;
		int featuresSize = 10;
		
		SimplifiedSeeker model = new SimplifiedSeeker(featuresSize, "1");
		
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel = new SimplifiedCABClusterMoldableModel<DiagonalMatrix, OnAxisVector>(model);

			
		MoldableModel<DiagonalMatrix, OnAxisVector> clusterModel = clusterMoldableModel.getClusterModel();
		float[] content = model.memory().getDiagonal().getContent();
		float[] contentCluster = clusterModel.memory().getDiagonal().getContent();
		
		for (int i = 0; i < content.length; i++) {
			assertEquals(content[i],contentCluster[i],0.0000001);
		}
		clusterMoldableModel.add(model);
		content = model.memory().getDiagonal().getContent();
		contentCluster = clusterModel.memory().getDiagonal().getContent();
		for (int i = 0; i < contentCluster.length; i++) {
			assertEquals(content[i],contentCluster[i],0.0000001);
		}
		SimplifiedSeeker model2 = new SimplifiedSeeker(featuresSize, "2");
		ClusterMoldableModel<DiagonalMatrix, OnAxisVector> clusterMoldableModel2 = new SimplifiedCABClusterMoldableModel<DiagonalMatrix, OnAxisVector>(model2);
		for (int x = 0; x < 10; x++) {
			Advisable<OnAxisVector> features=new OnAxisAdvisable(x);
			model.update(payoff,features );
			model2.update(payoff,features );
			clusterMoldableModel.getMembership(model, features, 0.1);
			clusterMoldableModel.getMembership(model2, features, 0.1);		
			clusterMoldableModel2.getMembership(model, features, 0.1);
			clusterMoldableModel2.getMembership(model2, features, 0.1);
			clusterMoldableModel.add(model);
			clusterMoldableModel.add(model2);
			clusterMoldableModel2.add(model);
			clusterMoldableModel2.add(model2);

	
			content = model.memory().getDiagonal().getContent();
			float[] content2 = model2.memory().getDiagonal().getContent();

	
			contentCluster = clusterModel.memory().getDiagonal().getContent();
			assertNotEquals(content[x],contentCluster[x]);
			assertNotEquals(content2[x],contentCluster[x]);
			assertArrayEquals(content, content2,0.0001f);

		}
		contentCluster = clusterMoldableModel.getClusterModel().memory().getDiagonal().getContent();
		float[] contentCluster2 = clusterMoldableModel2.getClusterModel().memory().getDiagonal().getContent();
		assertArrayEquals(contentCluster, contentCluster2,0.0001f);

	}
	

	
	
	
}
