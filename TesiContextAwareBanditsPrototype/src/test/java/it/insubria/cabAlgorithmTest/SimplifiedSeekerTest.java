package it.insubria.cabAlgorithmTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import it.insubria.cabAlgorithm.dataStructures.Advisable;
import it.insubria.cabAlgorithm.dataStructures.OnAxisAdvisable;
import it.insubria.cabAlgorithm.dataStructures.OnAxisVector;
import it.insubria.cabAlgorithm.dataStructures.SimplifiedSeeker;

public class SimplifiedSeekerTest {
	
	@Test
	public void addMemory(){
		int featuresSize = 10;
		float iterations=10;
		SimplifiedSeeker toTest=new SimplifiedSeeker(featuresSize, "10");
		SimplifiedSeeker toTest2=new SimplifiedSeeker(featuresSize, "10");
		for (int i = 0; i < featuresSize; i++) {
			toTest2.update(1, new OnAxisAdvisable(i));
		}

		for (int i = 0; i < iterations-1; i++) {
			toTest.addMemory(toTest2);
		}
		 float[] content = toTest.memory().getDiagonal().getContent();
		float[] inverseOfMemory = toTest.getInverseOfMemory().getDiagonal().getContent();
		for (int i = 0; i < content.length; i++) {
			assertEquals(iterations,content[i],0);
			assertEquals(1/iterations,inverseOfMemory[i],0.1);

		}
	}
	
	
	@Test
	public void confidenceBound(){
		int featuresSize = 10;
		SimplifiedSeeker toTest=new SimplifiedSeeker(featuresSize, "10");
		
		Advisable<OnAxisVector> anItem=new OnAxisAdvisable(0);
		int explotationParameter = 1;
		double confidenceBound = explotationParameter*toTest.getConfidenceBound(anItem);
		System.out.println(confidenceBound);
		toTest.update(0, anItem);
		confidenceBound = explotationParameter*toTest.getConfidenceBound(anItem);
		System.out.println("confidenceBound "+confidenceBound);
		toTest.update(0, anItem);
		Advisable<OnAxisVector> anOtherItem=new OnAxisAdvisable(1);
		confidenceBound = explotationParameter* toTest.getConfidenceBound(anOtherItem);
		System.out.println("confidenceBound "+confidenceBound);
		toTest.update(1, anItem);

		toTest.update(1, anItem);
	}
}
